// server.js

// set up ======================================================================
// get all the tools we need
var express = require('express.io');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var app = express();
var port = process.env.PORT || 8080;
var configDB = require('./config/database.js');

app.http().io();
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

app.configure(function () {
  app.use("/", express.static(__dirname + '/public'));
  app.use("/app", express.static(__dirname + '/admin/app'));
  app.use("/app/app", express.static(__dirname + '/admin/app'));
  app.use("/app/vendor", express.static(__dirname + '/admin/vendor'));
  app.use("/app/server", express.static(__dirname + '/admin/server'));
  app.use(express.cookieParser()); // read cookies (needed for auth)
  app.use(express.logger('dev')); // log every request to the console
  app.use(express.bodyParser()); // get information from html forms
  // required for passport
  app.use(express.session({secret: 'ilovescotchscotchyscotchscotch'})); // session secret
  app.use(passport.initialize());
  app.use(passport.session()); // persistent login sessions
  app.use(flash()); // use connect-flash for flash messages stored in sessio
});


// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// mobile api
require('./app/routes_mobile.js')(app, passport); // load mobile api

// api
require('./app/api.js')(app, express); // load api


// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
