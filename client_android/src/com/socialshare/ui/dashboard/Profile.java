package com.socialshare.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialshare.R;

public class Profile extends TabFragment {

    public static Profile instantiate(String tabTitle){
        Bundle bundle =  new Bundle();
        bundle.putString(ATTR_TITLE, tabTitle);
        Profile measureTF = new Profile();
        measureTF.setArguments(bundle);
        return measureTF;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View measureView = inflater.inflate(R.layout.measurement, container, false);
        return measureView;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(ATTR_TITLE);
    }
}
