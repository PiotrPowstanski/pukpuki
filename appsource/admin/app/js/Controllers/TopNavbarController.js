'use strict';
angular.module('angle').controller('TopNavbarController',
    ['$scope', '$rootScope',
        function ($scope, $rootScope) {

            $scope.notificationOpened = false;
            $scope.notifications = [];
            $scope.notificationCounter = 0;

            $rootScope.$on('notify.INVITATION', function (event, data) {
                var notification = {
                    type: 'INVITATION',
                    text: 'Zaproszenie do ' + data.groupName,
                    description: data.schoolName,
                    link: '#/app/invitations'
                };
                if (!$scope.notificationOpened) {
                    $scope.notificationCounter++;
                }

                $scope.notifications.unshift(notification)

            });

            $rootScope.$on('notify.INVITATION_ACCEPTED_OR_DECLINED', function (event, data) {
                var text = "";
                var type = ""
                if(data.acceptedOrDeclined === 'accepted'){
                    text = "Akceptacja zaproszenia do " + data.groupName + " (" + data.schoolName + ")";
                    type = "INVITATION_ACCEPTED";
                } else {
                    text = "Rezygnacja z zaproszenia do " + data.groupName + " (" + data.schoolName + ")";
                    type = "INVITATION_DECLINED";
                }
                var notification = {
                    type: type,
                    text: text,
                    description: 'Opiekun ucznia ' + data.studentName,
                    link: '#/app/groups'
                };
                if (!$scope.notificationOpened) {
                    $scope.notificationCounter++;
                }

                $scope.notifications.unshift(notification)

            });

            $scope.open = function () {
                $scope.notificationCounter = 0;
                $scope.notificationOpened = !$scope.notificationOpened;
            }

        }]);
