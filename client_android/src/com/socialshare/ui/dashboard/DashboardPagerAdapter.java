package com.socialshare.ui.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class DashboardPagerAdapter extends FragmentStatePagerAdapter{

    private List<TabFragment> fragments;

    public DashboardPagerAdapter(FragmentManager fm, List<TabFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public int getCount() {
        return this.fragments != null ? this.fragments.size() : 0;
    }

    @Override
    public Fragment getItem(int i) {
        return this.fragments != null ? this.fragments.get(i) : null;
    }
}
