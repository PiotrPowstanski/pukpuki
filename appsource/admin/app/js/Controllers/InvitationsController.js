'use strict';
angular.module('angle').controller('InvitationsController', [
  '$scope', '$rootScope', 'UserService', '$modal', 'toaster', '$interval', function ($scope, $rootScope, UserService, $modal, toaster, $interval) {

    /**
     * stores a list of invitations
     */
    $scope.invitations = [];

    $scope.init = function () {
      console.debug('InvitationsController initialization');

      //loads invitations
      UserService.getInvitations().then(
        function (data) {
          $scope.invitations = data;
        },
        function (err) {
          //nothing to do
        }
      );

      $rootScope.$on('notify.INVITATION', function (event, data) {
        UserService.getInvitations(data.id).then(
            function (dataInv) {
              var dataEl = dataInv.shift();
              dataEl.new = true;
              $scope.invitations.unshift(dataEl);
              $interval(function () {
                dataEl.new = false;
              }, 7000, 1);
            },
            function (err) {
              //nothing to do
            }
        );
      });
    }


    $scope.getInvitation = function (id) {
      for (var i = 0; $scope.invitations.length > 0; i++) {
        if ($scope.invitations[i]._id === id) {
          return $scope.invitations[i];
          break;
        }
      }
    }

    $scope.acceptanceChange = function (invitation) {
      $scope.open('sm', invitation);
    }
    /**
     * PART OF CODE RELATED TO
     * MODAL WINDOW OF INVITATION ACCEPTANCE
     */

    $scope.open = function (size, invitation) {

      var modalInstance = $modal.open({
        templateUrl: '/acceptance.html',
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          acceptanceStatus: function () {
            return invitation.invitationAccepted;
          },
          groupName: function () {
            return invitation.name;
          }
        }
      });

      var state = $('#modal-state');

      modalInstance.result.then(function () {

        if (!invitation.invitationAccepted) {
          UserService.declineInvitation(invitation.groupId, invitation.studentId).then(
            function () {
              toaster.pop('info', 'Info', 'Udało się zdeaktywować zaproszenie');
            },
            function () {
              invitation.accepted = !invitation.accepted;
              toaster.pop('error', 'Błąd', 'Nie udało się zdeaktywować zaproszenia');
            }
          );
        } else {
          UserService.acceptInvitation(invitation.groupId, invitation.studentId).then(
            function () {
              toaster.pop('info', 'Info', 'Udało się zaakceptować zaproszenie');
            },
            function () {
              invitation.accepted = !invitation.accepted;
              toaster.pop('error', 'Błąd', 'Nie udało się zaakceptować zaproszenia');
            }
          );
        }

      }, function (dismiss) {
        console.debug(dismiss);
        invitation.accepted = !invitation.accepted;
      });

    };

    var ModalInstanceCtrl = function ($scope, $modalInstance, acceptanceStatus, groupName) {

      $scope.acceptanceStatus = acceptanceStatus;
      $scope.groupName = groupName;

      $scope.ok = function () {
        $modalInstance.close();
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };
  }
]);
