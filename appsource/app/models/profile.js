var mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate');
var ProfileUtil = require('../utils/ProfileUtil');

var ownGroupMessageSchema = mongoose.Schema({
  creationDate: {type: Date, default: Date.now},
  text: String,
  status: {type: String, default: 'COMPOSED'}

}, {versionKey: false});

var invitationSchema = mongoose.Schema({
  _groupOwner : { type: mongoose.Schema.ObjectId, required: true},
  _group : { type: mongoose.Schema.ObjectId, required: true}
});

var ownGroupsSchema = mongoose.Schema({
  name: String,
  description : String,
  creationDate: {type: Date, default: Date.now}
}, {versionKey: false});

var profileSchema = mongoose.Schema({
  email: {type: String, required : true},
  // changed to false to avoid "ValidationError: Path `name` is required." error during GoogleOneTimeAuthCodeStrategy.
  name: {type: String, required : false},
  photoUrl: String,
  ownGroups: [ownGroupsSchema],
  invitations: [invitationSchema]
}, {versionKey: false});
profileSchema.plugin(findOrCreate);

profileSchema.pre('save', function (next) {
  if (this.name && this.isModified('name')) {
    this.photoUrl = "//dummyimage.com/300/000000/fff&text=" + ProfileUtil.createInitialsByUserName(this.name);
  }
  next();
});

module.exports = {
  OwnGroupMessage: mongoose.model('OwnGroupMessage', ownGroupMessageSchema),
  OwnGroup: mongoose.model('OwnGroup', ownGroupsSchema),
  Profile: mongoose.model('Profile', profileSchema)
}
