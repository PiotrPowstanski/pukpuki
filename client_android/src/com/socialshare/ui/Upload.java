package com.socialshare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;

/**
 * The Class Upload is the Fragment class that is launched when the user clicks
 * on Upload option in Left navigation drawer.
 */
public class Upload extends CustomFragment
{

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.upload, null);

		setTouchNClick(v.findViewById(R.id.btn1));
		setTouchNClick(v.findViewById(R.id.btn2));
		setTouchNClick(v.findViewById(R.id.btn3));
		setTouchNClick(v.findViewById(R.id.btn4));
		setTouchNClick(v.findViewById(R.id.btnSend));
		return v;
	}

}
