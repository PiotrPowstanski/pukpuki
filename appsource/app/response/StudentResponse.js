var ParentResponse = require("../response/ParentResponse");

function StudentResponse(student) {
  this.id = student._id;
  this.firstName = student.firstName;
  this.lastName = student.lastName;
  this.parents = [];
  this.addParents(student.parents);
}

StudentResponse.prototype.addParent = function(parent){
  var that = this;
  that.parents.push(new ParentResponse(parent));
};

StudentResponse.prototype.addParents = function(parents){
  var that = this;
  parents.forEach(function(parent){
    that.addParent(parent);
  });
};

module.exports = StudentResponse;


