var logger = require('winston');
var ProfileModel = require('../models/profile');
var OutboundMessage = require('../response/OutboundMessage');
var InboundMessage = require('../response/InboundMessage');
var GroupModel = require("../models/group");
var MessageModel = require("../models/message");
var GroupDoesntExistException = require('../exceptions/GroupDoesntExistException');
var MessageNotFoundException = require('../exceptions/MessageNotFoundException');
var InvocationException = require('../exceptions/InvocationException');
var ConfirmReadingException = require('../exceptions/ConfirmReadingException');
var ObjectId = require('mongoose').Types.ObjectId;
var mongooosePaginate = require('mongoose-paginate');
var contra = require('contra');
var _ = require("underscore");
var moment = require("moment");
function MessagesService() {

  /**
   * Possible
   */
  var periods = ['week', 'month', 'year'];

  function getEndOfPeriod(period){
    var now = moment();
    var periodToUse = 'month';
    var indexOfPeriod = periods.indexOf(period);
    if(indexOfPeriod !== -1){
      periodToUse = periods.indexOf(indexOfPeriod);
    }
    if('week' === periodToUse){
      return now.add(-1, 'weeks').startOf('week');
    } else if('year') {
      return now.add(-1, 'years').startOf('year');
    } else {
      return now.add(-1, 'months').startOf('month');
    }
  }

  function createMessage(userEmail, userProfileId, groupId, message, callback) {
    contra.waterfall(
      [
        /**
         * Fetches the group.
         *
         * @param next
         */
          function (next) {
          GroupModel.Group.find(
            {ownerProfile: userProfileId, _id: new ObjectId(groupId)}
          ).populate(
            'ownerProfile', 'name'
          ).populate(
            'students.parents.profile', 'name'
          ).exec(
            function (err, groups) {
              try {
                if (err || !groups || groups.length !== 1) {
                  next(new GroupDoesntExistException("Requested group couldn't be found."));
                } else {
                  next(null, groups.shift().toObject());
                }
              } catch (err) {
                next(err);
              }
            }
          );
        },

        /**
         * Filters student whose parents have accepted invitations.
         *
         * @param groupObj
         * @param next
         */
          function (groupObj, next) {
          groupObj.students = _.filter(groupObj.students, function (student) {
            student.parents = _.filter(student.parents, function (parent) {
              return parent.invitationAccepted === true;
            });
            return student.parents.length > 0;
          });

          if (groupObj.students.length > 0) {
            next(null, groupObj);
          } else {
            next(new InvocationException('There are no parents that accepted invitation'));
          }
        },

        /**
         * Creates a message.
         *
         * @param groupObj
         * @param next
         */
          function (groupObj, next) {

          var newMessage = new MessageModel.Message();
          newMessage.group = groupObj._id;
          newMessage.owner = groupObj.ownerProfile;
          newMessage.messageSubject = message.messageSubject;
          newMessage.messageText = message.messageText;

          groupObj.students.forEach(function (student) {
            student.parents.forEach(function (parent) {
              var messageParent = new MessageModel.MessageRecipient();
              messageParent.parent = parent._id;
              messageParent.student = student._id;
              messageParent.profile = parent.profile;
              newMessage.recipients.push(messageParent);
            });
          });

          newMessage.save(function (err) {
            if (err) {
              next(err);
            } else {
              next(null, new OutboundMessage(groupObj.students, newMessage.toObject()));
            }
          });
        }
      ],
      function (err, result) {
        callback(result, err);
      }
    );
  };

  function getOutboundMessages(userId, groupId, callback) {
    contra.waterfall([
      /**
       * Loads outbound messages.
       *
       * @param next
       */
        function (next) {
        MessageModel.Message.find(
          {group: new ObjectId(groupId), owner: userId},
          function (err, messages) {
            if (err) {
              next(new InvocationException("Can't load outbound messages"));
            } else {
              next(null, messages);
            }
          }
        )
      },

      /**
       * Loads the group
       *
       * @param messages
       * @param next
       */
        function (messages, next) {
        GroupModel.Group.find(
          {_id:groupId, ownerProfile: userId}
        ).populate(
          'ownerProfile', 'name'
        ).populate(
          'students.parents.profile', 'name'
        ).exec(
          function(err, groups){
            if (err || !groups || groups.length !== 1) {
              next(new GroupDoesntExistException("Requested group couldn't be found."));
            } else {
              next(null, messages, groups.shift());
            }
          }
        );
      },
      /**
       * Filters out student parents from group if they are not recipients in message.
       *
       * @param messages
       * @param group
       */
      function(messages, group, next){
        var responseMessages = [];
        messages.forEach(function(message){
          var students = group.toObject().students;
          students =_.filter(students, function(student){
            student.parents = _.filter(student.parents, function(parent){
              var messageRecipient =_.find(message.recipients, function(recipient){
                return recipient.parent.equals(parent._id);
              });
              return(messageRecipient ? true : false);
            });
            return student.parents.length > 0;
          });
          responseMessages.push(new OutboundMessage(students, message));
        });
        next(null, responseMessages);
      }
    ], function (err, result) {
      callback(result, err);
    });
  };

  function getInboundMessages(userProfileId, period, callback) {
    contra.waterfall([
      function (next) {
        var dateForm = getEndOfPeriod();
        MessageModel.Message.find(
          {'recipients.profile': new ObjectId(userProfileId), 'creationDate' : {$gt:dateForm}}
        ).populate('owner group').exec(
          function(error, messages){
            next(null, messages);
          }
        );
      }, function(messages, next){
        var result = [];
        messages.forEach(function(message){
          for (var i = 0; i < message.recipients.length; i++) {
            var recipient = message.recipients[i];
            if(recipient.profile.equals(userProfileId)){
              var student = message.group.students.id(recipient.student);
              var inboundMessage = new InboundMessage(
                message._id,
                message.group._id,
                message.messageSubject,
                message.messageText,
                message.creationDate,
                message.owner.name,
                student.firstName,
                student.lastName,
                recipient.parent
              );
              result.push(inboundMessage);
            }
          }
        });
        next(null, result)
      }
    ], function (err, result) {
      callback(result, err);
    })
  }

  function confirmReading(email, messageId, recipientId, callback) {
    try {
      contra.waterfall(
        [
          //fetches message
          function (next) {
            try {
              MessageModel.Message.find(
                {
                  _id: new ObjectId(messageId),
                  'recipients.recipientEmail': email,
                  'recipients._id': new ObjectId(recipientId)
                },
                function (err, message) {
                  if (err || !message || message.length !== 1) {
                    next(err ? err : new MessageNotFoundException("Message couldn't be found"));
                  } else {
                    next(null, message.shift());
                  }
                });
            } catch (err) {
              next(err);
            }
          },
          //confirms reading
          function (message, next) {
            try {
              //finds recipient
              var recipient = message.recipients.id(new ObjectId(recipientId));
              //sets up confirmation date
              if (typeof recipient === 'object') {
                //probably it was confirmed already
                if (!recipient.confirmationDate) {
                  recipient.confirmationDate = Date.now();
                  //update message
                  message.save(function (err) {
                    next(err);
                  });
                }
                //confirmed
                next(null, true);
              } else {
                next(new ConfirmReadingException("Couldn't confirm reading"));
              }
            } catch (err) {
              next(err);
            }
          }
        ],
        function (err, result) {
          if (err) {
            callback(null, err);
          } else {
            callback({confirmed: result === true});
          }
        }
      );
    } catch (err) {
      callback(null, new InvocationException("Message couldn't be confirmed"))
    }
  }

  return {
    createMessage: createMessage,
    getOutboundMessages: getOutboundMessages,
    getInboundMessages: getInboundMessages,
    confirmReading: confirmReading
  }
}

module.exports = new MessagesService();
