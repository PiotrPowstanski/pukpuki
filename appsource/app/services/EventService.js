var EventCreator = require("../common/EventCreator");

function EventService(express, request) {
    this.express = express;
    this.request = request;
    return this;
}

EventService.prototype.broadcast = function (email, event, data) {
    try {
        if (this.express.io.sockets.clients(email).length > 0) {
            this.express.io.room(email).broadcast('data', EventCreator.createEvent(event, data));
        }
    } catch (err) {
    }
}

module.exports = EventService;