function ProfileExistException(message) {
  this.message = message;
  this.name = "ProfileExistException";
}

module.exports = ProfileExistException;

