function ParentResponse(parent) {
  this.id = parent._id;
  this.invitationAccepted = parent.invitationAccepted;
  this.invitationDate = parent.invitationDate;
  this.parentEmail = parent.parentEmail;
  this.parentPhoneNumber = parent.parentPhoneNumber;
  this.parentName = parent.profile ? parent.profile.name : ""
}

ParentResponse.prototype.extendByConfirmation = function (confirmationDate) {
  var that = this;
  that.confirmationDate = confirmationDate;
};
module.exports = ParentResponse;



