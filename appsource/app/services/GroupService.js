var logger = require('winston');
var GroupModel = require("../models/group");
var GroupResponse = require("../response/GroupResponse");
var ProfileModel = require('../models/profile');
var InvocationException = require('../exceptions/InvocationException');
var GroupDoesntExistException = require('../exceptions/GroupDoesntExistException');
var InvalidParameterException = require('../exceptions/InvalidParameterException');
var ObjectId = require('mongoose').Types.ObjectId;
var contra = require('contra');
var _ = require("underscore");
var ValidationService = new require("../services/ValidationService");
var validator = require('validator');
var Student = require("../response/StudentResponse");
var UserProfileService = new require("../services/UserProfileService");

function GroupService() {

  function getGroupDocument(query, callback) {
    try {
      GroupModel.Group.find(query,
        function (err, group) {
          try {
            if (err || !group || group.length !== 1) {
              callback(null, new GroupDoesntExistException("Requested group couldn't be found."));
            } else {
              callback(group.shift());
            }
          } catch (err) {
            callback(null, err);
          }
        }
      );
    } catch (err) {
      callback(null, err);
    }
  }

  /**
   * Creates new group.
   *
   * @param email
   * @param group {name - group/grade name, description - school/playschool name}
   * @param callback
   */
  function createGroup(userEmail, userId, groupObject, callback) {
    try {
      //creates new group
      var newGroupModel = new GroupModel.Group();
      newGroupModel.name = groupObject.name;
      newGroupModel.description = groupObject.description;
      newGroupModel.ownerEmail = userEmail;
      newGroupModel.ownerProfile = userId;

      //saves the group
      newGroupModel.save(function (err) {
        try {
          callback(!err ? newGroupModel : null, err);
        } catch (err) {
          callback(null, new InvocationException("New group couldn't be created"));
        }
      });
    } catch (err) {
      logger.log('error', err);
      callback(null, new InvocationException("New group couldn't be created"));
    }
  }

  /**
   * Invites student and its parent to the group
   *
   * @param email
   * @param groupId
   * @param invitationObject
   * @param callback
   */
  function inviteMember(email, groupId, invitationObject, callback) {
    contra.waterfall(
      [
        //fetches group
        function (next) {
          try {
            getGroupDocument({ownerEmail: email, _id: new ObjectId(groupId)},
              function (group, err) {
                next(err, group);
              });
          } catch (err) {
            next(new InvocationException(err));
          }
        },
        //validates whether the email is correct
        function (group, next) {
          try {
            ValidationService.validateEmail(invitationObject.parentEmail,
              function (isValid, err) {
                next(err, group);
              });
          } catch (err) {
            next(new InvocationException(err));
          }
        },
        //creates new member and saves
        function (group, next) {
          try {
            //prepares new student object
            var newStudent = new GroupModel.Student();
            newStudent.firstName = invitationObject.firstName;
            newStudent.lastName = invitationObject.lastName;
            //prepares parent of new student
            var newParent = new GroupModel.Parent();
            newParent.parentPhoneNumber = invitationObject.parentPhoneNumber;
            newParent.parentEmail = invitationObject.parentEmail;
            //adds parent to student
            newStudent.parents.push(newParent);
            //adds student to group
            group.students.push(newStudent);
            //saves new student
            group.save(function (err) {
              if (!err) {
                next(null, new Student(newStudent));
              } else {
                next(err)
              }
            });
          } catch (err) {
            next(err);
          }
        }
      ], function (err, newStudent) {
        callback(newStudent, err ? new InvocationException("New member couldn't be invited.") : null);
      }
    );
  }

  /**
   * Fetches group data
   *
   * @param email
   * @param groupId
   * @param callback
   */
  function getGroup(email, groupId, callback) {
    contra.waterfall(
      [
        function (next) {
          try {
            GroupModel.Group.find(
              {ownerEmail: email, _id: new ObjectId(groupId)}
            ).populate('students.parents.profile', 'name').exec(
              function (err, group) {
                try {
                  if (err || !group || group.length !== 1) {
                    next(new GroupDoesntExistException("Requested group couldn't be found."));
                  } else {
                    next(null, new GroupResponse(group.shift()));
                  }
                } catch (err) {
                  next(err);
                }
              }
            );
          } catch (err) {
            next(err);
          }
        }
      ],
      function (err, group) {
        callback(group, err ? new InvocationException("Requested group couldn't be fetched.") : null);
      }
    );
  }

  function getParent(parentId, callback){
    GroupModel.Parent.find({_id: new ObjectId(parentId)}, function(err, parent){
      callback(parent, err);
    });
  }

  return {
    createGroup: createGroup,
    inviteMember: inviteMember,
    getGroup: getGroup,
    getParent:getParent
  }
}

module.exports = new GroupService();
