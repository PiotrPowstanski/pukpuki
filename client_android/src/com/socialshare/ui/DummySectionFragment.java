package com.socialshare.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socialshare.R;

public class DummySectionFragment extends Fragment {

    public static final String ARGS_POSITION = "args_position";
    public static final String ARG_TITLE = "args_title";

    public static Fragment createInstance(int position){
        Bundle args = new Bundle();
        args.putInt(DummySectionFragment.ARGS_POSITION, position);

        DummySectionFragment dummyFragment = new DummySectionFragment();
        dummyFragment.setArguments(args);
        return dummyFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_section_dummy, container, false);

        TextView textView = (TextView) rootView.findViewById(android.R.id.text1);
        textView.setText(getActivity().getString (R.string.dummy_section_text, getArguments().getInt(ARGS_POSITION)));

        return rootView;
    }
}
