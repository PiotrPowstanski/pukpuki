function DocumentNotFoundException(message) {
  this.message = message;
  this.name = "DocumentNotFoundException";
}

module.exports = DocumentNotFoundException;
