angular.module('angle').factory('UserService', ['$http', '$q', 'OutboundMessage', function ($http, $q, OutboundMessage) {

  function OwnGroupComparatorByName(a, b) {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    // a must be equal to b
    return 0;
  };

  return {
    /**
      _______ ________________   _______ _______ _______ _________________       _______
     (  ____ (  ____ \__   __/  (  ____ (  ____ (  ___  (  ____ \__   __( \     (  ____ \
     | (    \| (    \/  ) (     | (    )| (    )| (   ) | (    \/  ) (  | (     | (    \/
     | |     | (__      | |     | (____)| (____)| |   | | (__      | |  | |     | (__
     | | ____|  __)     | |     |  _____|     __| |   | |  __)     | |  | |     |  __)
     | | \_  | (        | |     | (     | (\ (  | |   | | (        | |  | |     | (
     | (___) | (____/\  | |     | )     | ) \ \_| (___) | )     ___) (__| (____/| (____/\
     (_______(_______/  )_(     |/      |/   \__(_______|/      \_______(_______(_______/
     */
    getUserProfile: function () {
      console.debug("Calling getUserProfile()");
      return $http({
        method: 'GET',
        url: '/api/profile',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function (response) {
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          var result = response.data.result;
          result.ownGroups.sort(OwnGroupComparatorByName);
          return result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function (err) {
        // something went wrong
        return $q.reject(err);
      });
    },

    /**
     *
      _______ _______ _______ _______________________    _______ _______ _______         _______
     (  ____ (  ____ (  ____ (  ___  \__   __(  ____ \  (  ____ (  ____ (  ___  |\     /(  ____ )
     | (    \| (    )| (    \| (   ) |  ) (  | (    \/  | (    \| (    )| (   ) | )   ( | (    )|
     | |     | (____)| (__   | (___) |  | |  | (__      | |     | (____)| |   | | |   | | (____)|
     | |     |     __|  __)  |  ___  |  | |  |  __)     | | ____|     __| |   | | |   | |  _____)
     | |     | (\ (  | (     | (   ) |  | |  | (        | | \_  | (\ (  | |   | | |   | | (
     | (____/| ) \ \_| (____/| )   ( |  | |  | (____/\  | (___) | ) \ \_| (___) | (___) | )
     (_______|/   \__(_______|/     \|  )_(  (_______/  (_______|/   \__(_______(_______|/

     * @param newGroup
     * @returns {*}
     */
    createGroup: function(newGroup){
      console.debug('Calling createGroup()');
      return $http({
        method: 'POST',
        url: '/api/groups',
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        data: newGroup
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    createGroupNew: function(newGroup){
      console.debug('Calling createGroup()');
      return $http({
        method: 'POST',
        url: '/api/groups',
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        data: newGroup
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    /**
      _______ _______ _______    _______ _______ _______         _______
     (  ____ (  ____ (  ____ )  (  ____ (  ____ (  ___  |\     /(  ____ )
     | (    \| (    \| (    )|  | (    \| (    )| (   ) | )   ( | (    )|
     | (__   | (__   | (____)|  | |     | (____)| |   | | |   | | (____)|
     |  __)  |  __)  |     __)  | | ____|     __| |   | | |   | |  _____)
     | (     | (     | (\ (     | | \_  | (\ (  | |   | | |   | | (
     | )     | (____/| ) \ \__  | (___) | ) \ \_| (___) | (___) | )
     |/      (_______|/   \__/  (_______|/   \__(_______(_______|/

     * @param groupId
     * @returns {*}
     */
    getGroup: function(groupId){
      console.debug('Calling getGroup()');
      return $http({
        method: 'GET',
        url: '/api/groups/' + groupId,
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
          if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
            return response.data.result;
          } else {
            // invalid response
            return $q.reject(response.data.error);
          }
      }, function(err){
        return $q.reject(err);
      });
    },
    /**
     __________              ________________________    _______ _______ _______ ______  _______ _______
     \__   __( (    /|\     /\__   __\__   __(  ____ \  (       (  ____ (       (  ___ \(  ____ (  ____ )
        ) (  |  \  ( | )   ( |  ) (     ) (  | (    \/  | () () | (    \| () () | (   ) | (    \| (    )|
        | |  |   \ | | |   | |  | |     | |  | (__      | || || | (__   | || || | (__/ /| (__   | (____)|
        | |  | (\ \) ( (   ) )  | |     | |  |  __)     | |(_)| |  __)  | |(_)| |  __ ( |  __)  |     __)
        | |  | | \   |\ \_/ /   | |     | |  | (        | |   | | (     | |   | | (  \ \| (     | (\ (
     ___) (__| )  \  | \   / ___) (___  | |  | (____/\  | )   ( | (____/| )   ( | )___) | (____/| ) \ \__
     \_______|/    )_)  \_/  \_______/  )_(  (_______/  |/     \(_______|/     \|/ \___/(_______|/   \__/

     * @param groupId - group's id to which the new member have to be added
     * @param member - email
     * @returns {*}
     */
    inviteMember: function(groupId, member){
      console.debug('Calling inviteMember');
      return $http({
        method: 'POST',
        url: '/api/groups/' + groupId + '/invite',
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        data: member
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    /**
       /$$$$$$            /$$           /$$$$$$                   /$$  /$$               /$$    /$$
      /$$__  $$          | $$          |_  $$_/                  |__/ | $$              | $$   |__/
     | $$  \__/ /$$$$$$ /$$$$$$          | $$  /$$$$$$$ /$$    /$$/$$/$$$$$$   /$$$$$$ /$$$$$$  /$$ /$$$$$$ /$$$$$$$  /$$$$$$$
     | $$ /$$$$/$$__  $|_  $$_/          | $$ | $$__  $|  $$  /$$| $|_  $$_/  |____  $|_  $$_/ | $$/$$__  $| $$__  $$/$$_____/
     | $$|_  $| $$$$$$$$ | $$            | $$ | $$  \ $$\  $$/$$/| $$ | $$     /$$$$$$$ | $$   | $| $$  \ $| $$  \ $|  $$$$$$
     | $$  \ $| $$_____/ | $$ /$$        | $$ | $$  | $$ \  $$$/ | $$ | $$ /$$/$$__  $$ | $$ /$| $| $$  | $| $$  | $$\____  $$
     |  $$$$$$|  $$$$$$$ |  $$$$/       /$$$$$| $$  | $$  \  $/  | $$ |  $$$$|  $$$$$$$ |  $$$$| $|  $$$$$$| $$  | $$/$$$$$$$/
     \______/ \_______/  \___/        |______|__/  |__/   \_/   |__/  \___/  \_______/  \___/ |__/\______/|__/  |__|_______/
     */
    getInvitations: function(){
      console.debug('Calling getInvitations');
      return $http({
        method: 'GET',
        url: '/api/invitations',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    acceptInvitation: function(groupId, studentId){
      console.debug('Calling acceptInvitation');
      return $http({
        method: 'GET',
        url: '/api/invitations/' + groupId + '/' + studentId + '/accept',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error && response.data.result && response.data.result.accepted) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    declineInvitation: function(groupId, studentId){
      console.debug('Calling declineInvitation');
      return $http({
        method: 'GET',
        url: '/api/invitations/' + groupId + '/' + studentId + '/decline',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error && response.data.result && response.data.result.declined) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    createMessage: function(groupId, message){
      console.debug('Calling createMessage');
      return $http({
        method: 'POST',
        url: '/api/messages',
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        data: {groupId: groupId, message: message}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return  new OutboundMessage.build(response.data.result);
        } else {
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    getOutboundMessages: function (groupId) {
      console.debug('Calling getOutboundMessages');
      return $http({
        method: 'GET',
        url: '/api/groups/' + groupId + '/messages/outbound',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result.map(OutboundMessage.build).filter(Boolean);
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    getInboundMessages: function(){
      console.debug('Calling getInboundMessages');
      return $http({
        method: 'GET',
        url: '/api/inboundmessages',
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          // invalid response
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },

    confirmReading: function(messageId, recipientId){
      console.debug('Calling confirmReading');
      return $http({
        method: 'GET',
        url: '/api/inboundmessages/' + messageId + '/confirm/' + recipientId,
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error && response.data.result.confirmed === true) {
          return;
        } else {
          // invalid response
          return $q.reject();
        }
      }, function(){
        return $q.reject();
      });
    },
    inviteParent: function(groupId, studentId, parent){
      console.debug('Calling inviteParent');
      return $http({
        method: 'POST',
        url: '/api/groups/' + groupId + '/students/' + studentId + '/parents',
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        data: parent
      }).then(function(response){
        if (response.status === 200 && typeof response.data === 'object' && !response.data.error) {
          return response.data.result;
        } else {
          return $q.reject(response.data.error);
        }
      }, function(err){
        return $q.reject(err);
      });
    },
    getOwnGroupComparatorByName:OwnGroupComparatorByName
  }
}]);
