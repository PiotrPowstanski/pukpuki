package com.socialshare.ui.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.socialshare.R;
import com.socialshare.model.Data;
import com.socialshare.model.InvitationData;

import java.util.ArrayList;
import java.util.Locale;

public class InvitationsTabFragment extends TabFragment {

   // private VerticalViewPager viewPager;
   private ArrayList<InvitationData> actList;

    public static InvitationsTabFragment instantiate(String tabTitle){
        Bundle bundle =  new Bundle();
        bundle.putString(ATTR_TITLE, tabTitle);
        InvitationsTabFragment planTF = new InvitationsTabFragment();
        planTF.setArguments(bundle);

        return planTF;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.plan, container, false);
        loadActivities();
        ListView list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(new CutsomAdapter());
     //   viewPager = (VerticalViewPager) view.findViewById(R.id.scheduleViewPager);
     //   viewPager.setAdapter(new DummyAdapter(getFragmentManager()));
        return view;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(ATTR_TITLE);
    }

    public class DummyAdapter extends FragmentStatePagerAdapter {

        public DummyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "PAGE 1";
                case 1:
                    return "PAGE 2";
                case 2:
                    return "PAGE 3";
            }
            return null;
        }

    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            TextView rootView = new TextView(inflater.getContext());
            rootView.setText(R.string.app_name);
            return rootView;
        }


    }

    private class CutsomAdapter extends BaseAdapter
    {

        /* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
        @Override
        public int getCount()
        {
            return actList.size();
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
        @Override
        public InvitationData getItem(int arg0)
        {
            return actList.get(arg0);
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
        @Override
        public long getItemId(int arg0)
        {
            return arg0;
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        @Override
        public View getView(int pos, View v, ViewGroup arg2)
        {
            if (v == null){
                v = LayoutInflater.from(getActivity()).inflate(
                        R.layout.invitation_item, null);

            }

            TextView lbl = (TextView) v.findViewById(R.id.lbl1);
            lbl.setText(getItem(pos).getStudent());

            lbl = (TextView) v.findViewById(R.id.lbl2);
            lbl.setText(getItem(pos).getGroupName());

            lbl = (TextView) v.findViewById(R.id.lbl3);
            lbl.setText(getItem(pos).getSchoolName());

            ImageView img = (ImageView) v.findViewById(R.id.img1);
            img.setImageResource(getItem(pos).getProfileImage());

            CheckBox tb =(CheckBox) v.findViewById(R.id.checkBox1);
            tb.setChecked(getItem(pos).isAccepted());

            lbl = (TextView) v.findViewById(R.id.lbl4);
            lbl.setText(getItem(pos).getInvitationDate());



//            img = (ImageView) v.findViewById(R.id.img2);
//            img.setImageResource(getItem(pos).getImage2());
            return v;
        }

    }

    private void loadActivities()
    {
        actList = new ArrayList<InvitationData>();
        actList.add(new InvitationData("Klasa 1a", "SP nr 1 w Wysokiej", "Krzysztof Powstański", "12.12.2014", true, R.drawable.user1));
        actList.add(new InvitationData("Klasa 2a", "SP nr 1 w Wysokiej", "Anna Powstańska", "12.12.2014", true, R.drawable.user1));
        actList.add(new InvitationData("Klasa 3a", "SP nr 1 w Wysokiej", "Beniamin Powstański", "12.12.2014", true, R.drawable.user1));
        actList.add(new InvitationData("Klasa 4a", "SP nr 1 w Wysokiej", "Zofia Powstańska", "12.12.2014", true, R.drawable.user1));

    }
}
