package com.socialshare.custom;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.Arrays;

/**
 * Created by sunteam on 09.03.2014.
 */
public class TabCustomFragment extends CustomFragment {

    public static String ATTR_LABEL = "label";
    public static String ATTR_ICON = "icon";
    public static String ATTR_TAG = "tag";

    private CharSequence label;
    private Drawable icon;
    private String tag;

}
