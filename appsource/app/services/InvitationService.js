var ParentResponse = require("../response/ParentResponse")
var GroupModel = require("../models/group");
var ProfileModel = require("../models/profile");
var InvitationNotFoundException = require('../exceptions/InvitationNotFoundException');
var InvocationException = require('../exceptions/InvocationException');
var ObjectId = require('mongoose').Types.ObjectId;
var ValidationService = new require("../services/ValidationService");
var GroupDoesntExistException = require('../exceptions/GroupDoesntExistException');
var UserProfileService = new require("../services/UserProfileService");
var contra = require('contra');
var InvitationResponse = require("../response/InvitationResponse")
var _ = require("underscore");

function InvitationService() {

  function getInvitations(userEmail, userId, callback) {
    contra.waterfall([
      /**
       * gets groups which the user belongs to
       *
       * @param next
       */
        function (next) {
        GroupModel.Group.find(
          {$or: [{'students.parents.parentEmail': userEmail}, {'students.parents.profile': userId}]}
        ).populate('ownerProfile', 'name').exec(
          function (err, groups) {
            if (err) {
              next(err);
            } else {
              next(null, groups);
            }
          });
      },

      /**
       * creates invitation items
       *
       * @param groups
       * @param next
       */
        function (groups, next) {
        var invitations = [];
        //TODO should we use non-blocking loop instead?
        groups.forEach(function (group) {
          var groupObj = group.toObject();
          groupObj.students.forEach(function (student) {
            student.parents.forEach(function (parent) {
              if ((parent.profile && parent.profile.equals(userId)) || parent.parentEmail === userEmail) {
                var invitationResponse = new InvitationResponse(
                  groupObj._id.toString(),
                  groupObj.name,
                  groupObj.description,
                  groupObj.ownerProfile ? groupObj.ownerProfile.name : null,
                  student._id,
                  student.firstName,
                  student.lastName,
                  parent.invitationDate,
                  parent.invitationAccepted
                );
                invitations.push(invitationResponse);
              }
            });
          });
        });
        next(null, invitations);
      }
    ], function (err, invitations) {
      callback(invitations, err);
    });
  }

  function getInvitationsByEmail(email, memberId, callback) {
    if (!callback) {
      callback = memberId;
      memberId = null;
    }

    contra.waterfall(
      [
        //fetches the groups the user has been invited to join
        function (next) {
          var query = {'members.memberEmail': email};
          var options = {_id: 1, ownerEmail: 1, description: 1, name: 1, creationDate: 1, 'members': 1};
          if (memberId) {
            query = {'members.memberEmail': email, 'members._id': memberId};
            options = {_id: 1, ownerEmail: 1, description: 1, name: 1, creationDate: 1, 'members': 1};
          }
          try {
            GroupModel.Group.find(
              query,
              options,
              function (err, groups) {
                try {
                  if (err || !groups) {
                    next(new InvitationNotFoundException("Invitations couldn't be fetched"));
                  } else {
                    var invitations = [];
                    var emails = [];
                    for (var i = 0; groups.length > i; i++) {
                      var group = groups[i];
                      var members = _.where(group.members, {memberEmail: email});
                      for (var j = 0; members.length > j; j++) {
                        var member = members[j];
                        if (memberId == null || memberId != null && member._id.toString() == memberId) {
                          var invitationObj = {};
                          invitationObj.groupId = JSON.stringify(group._id);
                          invitationObj.groupName = group.name;
                          invitationObj.schoolName = group.description;
                          invitationObj.groupOwnerEmail = group.ownerEmail;
                          invitationObj.creationDate = member.invitationDate;
                          invitationObj.id = member._id;
                          invitationObj.studentName = member.memberDescription;
                          invitationObj.accepted = member.invitationAccepted;
                          invitations.push(invitationObj);
                          if (!_.contains(email, group.ownerEmail)) {
                            emails.push(group.ownerEmail);
                          }
                        }
                      }
                    }
                    next(null, invitations, emails);
                  }
                } catch (err) {
                  next(err);
                }
              }
            );
          } catch (err) {
            next(err);
          }
        },
        //fetches profiles of group owners
        function (invitations, emails, next) {
          try {
            ProfileModel.Profile.find(
              {'email': {$in: emails}},
              {email: 1, name: 1},
              function (err, profiles) {
                try {
                  if (err) {
                    next(err);
                  } else {
                    next(null, invitations, profiles);
                  }
                } catch (err) {
                  next(err);
                }
              }
            );
          } catch (err) {
            next(err);
          }
        },
        //fill in invitations with owner name
        function (invitations, profiles, next) {
          try {
            for (var i = 0; invitations.length > i; i++) {
              var profile = _.where(profiles, {'email': invitations[i].groupOwnerEmail});
              if (profile.length !== 1) {
                next(new InvocationException("Can't get group owner profile"));
              }
              invitations[i].groupOwnerName = profile[0].name;
            }
            next(null, invitations);
          } catch (err) {
            next(err);
          }
        }
      ],
      function (err, result) {
        callback(result, err ? new InvocationException("Outbound messages couldn't be found") : null);
      }
    );
  }

  function acceptOrDeclineInvitation(userEmail, userProfile, groupId, studentId, action, callback) {
    var accept = action === 'accept';

    GroupModel.Group.find(
      {_id: new ObjectId(groupId)},
      function (err, groups) {
        if (err || !groups || groups.length !== 1) {
          winston.log('error', "Group for id '%s' couldn't be found or error occurred or found to many groups.", groupId);
          callback(null, err ? err : new InvitationNotFoundException("Invitation couldn't be found"));
        } else {
          var doSave = false;
          var group = groups.shift();
          var student = group.students.id(new ObjectId(studentId));
          if (student && student.parents) {
            for (var i = 0; i < student.parents.length; i++) {
              var parent = student.parents[i];
              if (parent && parent.profile && parent.profile.equals(userProfile)) {
                parent.invitationAccepted = accept ? true : false;
                doSave = true;
              } else if (parent && parent.parentEmail === userEmail) {
                parent.profile = userProfile;
                parent.invitationAccepted = accept ? true : false;
                doSave = true;
              }
            }
          }
          if (doSave) {
            group.save(function (err) {
              if (err) {
                callback(null, new InvocationException("Invitation acceptance couldn't be processed"));
              } else {
                callback(accept ? {accepted: true} : {declined: true});
              }
            });
          } else {
            callback(null, new InvocationException("Invitation acceptance couldn't be processed"));
          }
        }
      }
    );
  }

  function inviteParent(email, groupId, studentId, parent, callback) {
    contra.waterfall(
      [
        //validates whether parent email is correct
        function (next) {
          try {
            ValidationService.validateEmail(parent.parentEmail,
              function (isValid, err) {
                next(err);
              });
          } catch (err) {
            next(new InvocationException("ParentResponse couldn't be invited."));
          }
        },
        function (next) {
          GroupModel.Group.find(
            {ownerEmail: email, _id: new ObjectId(groupId), 'students._id': new ObjectId(studentId)},
            function (err, groups) {
              try {
                if (err || !groups || groups.length !== 1) {
                  next(new GroupDoesntExistException("ParentResponse couldn't be invited."));
                } else {
                  next(null, groups.shift());
                }
              } catch (err) {
                next(new InvocationException("ParentResponse couldn't be invited."));
              }
            }
          );
        },
        function (group, next) {
          try {
            var parentResp = new GroupModel.Parent();
            parentResp.parentEmail = parent.parentEmail;
            parentResp.parentPhoneNumber = parent.parentPhoneNumber;
            group.students.id(new ObjectId(studentId)).parents.push(parent);
            group.save(function (err) {
              next(err, new ParentResponse(parentResp));
            });
          } catch (err) {
            next(new InvocationException("ParentResponse couldn't be invited."));
          }
        }
      ],
      function (err, response) {
        callback(response, err);
      }
    );
  }

  return {
    getInvitationsByEmail: getInvitationsByEmail,
    acceptOrDeclineInvitation: acceptOrDeclineInvitation,
    inviteParent: inviteParent,
    getInvitations: getInvitations
  }
}

module.exports = new InvitationService();
