module.exports = {
    createEvent : function(event, data){
        return {
            event : event,
            data: data
        };
    },
    events : {
        INVITATION : "INVITATION",
        INVITATION_ACCEPTED_OR_DECLINED : "INVITATION_ACCEPTED_OR_DECLINED"
    }
};
