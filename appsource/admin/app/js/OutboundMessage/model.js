angular.module('angle').factory('OutboundMessage', [function(){

  function OutboundMessage(message){
    this.id = message.id;
    this.subject = message.subject;
    this.text = message.text;
    this.creationDate = message.creationDate;
    this.confirmationInfo = {
      loaded : false,
      studentsWithConfirmation : [],
      studentsWithoutConfirmation : [],
      allRecipients:0,
      confirmedRecipients:0
    };
    this.students = message.students.map(Student.build).filter(Boolean);
    this.loadConfirmationInfo();
  }

  function Student(student){
    this.id = student.id;
    this.firstName = student.firstName;
    this.lastName = student.lastName;
    this.parents = student.parents.map(Parent.build).filter(Boolean);
  }

  function Parent(parent){
    this.id = parent.id;
    this.invitationAccepted = parent.invitationAccepted;
    this.parentEmail = parent.parentEmail;
    this.parentPhoneNumber = parent.parentPhoneNumber;
    this.parentName = parent.parentName;
    this.confirmationDate = parent.confirmationDate;
  }

  Student.prototype.getFullName = function(){
    var that = this;
    return that.firstName + " " + that.lastName;
  };

  Parent.build = function(message){
    return new Parent(message);
  };

  Student.build = function(message){
    return new Student(message);
  };

  OutboundMessage.build = function(message){
    return new OutboundMessage(message);
  };

  OutboundMessage.prototype.loadConfirmationInfo = function(){
    var that = this;

    if(!that.confirmationInfo.loaded){
      for (var i = 0; i < that.students.length; i++) {
        var student = that.students[i];
        var accepted = false;
        for (var j = 0; j < student.parents.length; j++) {
          var parent = student.parents[j];
          that.confirmationInfo.allRecipients++;
          if(parent.confirmationDate){
            accepted = true;
            that.confirmationInfo.confirmedRecipients++;
          }
        }
        if(accepted){
          that.confirmationInfo.studentsWithConfirmation.push(student.getFullName());
        } else {
          that.confirmationInfo.studentsWithoutConfirmation.push(student.getFullName());
        }
      }
      that.confirmationInfo.loaded = true;
    }
    //var result = {accepted : [], notAccepted: []};
    //for (var i = 0; i < that.students.length; i++) {
    //  var student = that.students[i];
    //  var accepted = false;
    //  for (var j = 0; j < student.parents.length; j++) {
    //    var parent = student.parents[j];
    //    if(parent.confirmationDate){
    //      accepted = true;
    //      break;
    //    }
    //  }
    //  if(accepted){
    //    result.accepted.push(student.getFullName());
    //  } else {
    //    result.notAccepted.push(student.getFullName());
    //  }
    //}
    //result.accepted = result.accepted.join(', ');
    //result.notAccepted = result.notAccepted.join(', ');

    return that.confirmationInfo;
  };

  return OutboundMessage;
}]);
