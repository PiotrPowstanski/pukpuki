function InvitationResponse(groupId,
                            groupName,
                            schoolName,
                            teacherName,
                            studentId,
                            studentFirstName,
                            studentLastName,
                            invitationDate,
                            invitationAccepted) {
  this.groupId = groupId;
  this.groupName = groupName;
  this.schoolName = schoolName;
  this.teacherName = teacherName;
  this.studentId = studentId;
  this.studentFirstName = studentFirstName;
  this.studentLastName = studentLastName;
  this.invitationDate = invitationDate;
  this.invitationAccepted = invitationAccepted
}

module.exports = InvitationResponse;


