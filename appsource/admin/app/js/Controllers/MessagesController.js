'use strict';
angular.module('angle').controller('MessagesController', ['$window', '$http', '$scope', 'APP_MEDIAQUERY', function ($window, $http, $scope, mq) {

  $scope.isMenuSticked = false;

  $scope.$on('$viewContentLoaded',
    function (event, viewConfig) {

      var $win = $(window);

      var stickyMenu = $('#groups-sticky-menu');
      var stickyOpts = {topSpacing:70, responsiveWidth: true, getWidthFrom: '.groups-sticky-menu-width'};

      doStickyGroupMenu();

      $win.on('resize', function(){
        doStickyGroupMenu();
      });

      function doStickyGroupMenu(){
        if( $win.width() < mq.tablet){
          if($scope.isMenuSticked){
            stickyMenu.unstick();
            $scope.isMenuSticked = false;
          }
        } else {
          if(!$scope.isMenuSticked){
            stickyMenu.sticky(stickyOpts);
            $scope.isMenuSticked = true;
          }
        }
      }

    });
}]);
