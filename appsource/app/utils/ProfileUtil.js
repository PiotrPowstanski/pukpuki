'use strict';

function createInitialsByUserName(name) {
  var splitNames = name.split(' ');
  var initials = '';
  if (splitNames.length > 0) {
    initials += splitNames.shift().toUpperCase();
  }
  if (splitNames.length > 0) {
    initials += splitNames.pop().toUpperCase();
  }
  return initials;
};

module.exports = {
  createInitialsByUserName : createInitialsByUserName
};
