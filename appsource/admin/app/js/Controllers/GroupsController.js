'use strict';
angular.module('angle').controller('GroupsController',
  ['$window', '$http', '$scope', 'APP_MEDIAQUERY', "$modal", "UserService", '$rootScope', '$state', 'toaster',
    function ($window, $http, $scope, mq, $modal, UserService, $rootScope, $state, toaster) {

      /**
       * indicates whether the sticky
       * menu is already stickied in order
       * to avoid unnecessary calls
       */
      $scope.isMenuSticked = false;

      /**
       *  indicates selected group
       */
      $scope.activeGroup = null;

      /**
       * Property that stores own group of user.
       * That is only reference to user profile
       * that is stored in the context of application
       *
       * TODO needs to be checked somehow whether
       * TODO the profile is already loaded form server
       */
      $scope.groups = null;


      $scope.newStudent = {firstName: "", lastName: "", parentEmail: "", parentPhoneNumber: ""};

      /**
       * initializes the controller
       */
      $scope.init = function () {
        console.debug('GroupsController initialization');
        //fetch groups from profile
        $scope.groups = $rootScope.user.ownGroups;
        //open first group if any
        if ($scope.groups && $scope.groups.length > 0) {
          $scope.openGroup($scope.groups[0]);
        }

        $rootScope.$on('notify.INVITATION_ACCEPTED_OR_DECLINED', function (event, data) {
          if ($scope.activeGroup) {
            for (var i = 0; $scope.activeGroup.members.length > i; i++) {
              var member = $scope.activeGroup.members[i];
              if (data.memberId == member._id) {
                member.invitationAccepted = data.acceptedOrDeclined === 'accepted' ? true : false;
                break;
              }
            }
          }
        });
      }

      $scope.headerInfo = function(){
        if($scope.activeGroup && $scope.activeGroup.students){
          var countParents = 0;
          var countAcceptations = 0;
          for(var i = 0; $scope.activeGroup.students.length > i ; i++){
            var student = $scope.activeGroup.students[i];
            if(student.parents){
              for(var j = 0; student.parents.length > j ; j++){
                countParents++;
                if(student.parents[j].invitationAccepted){
                  countAcceptations++;
                }
              }
            }
          }
          return {students: $scope.activeGroup.students.length, parents : countParents, acceptations : countAcceptations};
        } else {
          return {students: "", parents : "", acceptations : ""};
        }
      };

      /**
       * opens nested view that shows details
       * of selected view
       * @param groupId - id of group to be selected
       */
      $scope.openGroup = function (group) {
        UserService.getGroup(group._id).then(
          function (data) {
            $scope.activeGroup = data;
          },
          function () {
            toaster.pop('error', 'Error', 'Nie udało się pobrać danych dla : ' + group.name);
          }
        );
        //$scope.activeGroup = groupId;
        //$state.go('app.groups.item', {item: groupId}, {location: true});
      };

      /**
       * returns reference to requested group
       * @param group id
       * @returns group object
       */
      $scope.getGroupDetails = function (groupId) {
        if ($scope.groups) {
          for (var i = 0; $scope.groups.length > i; i++) {
            if ($scope.groups[i]._id === groupId) {
              return $scope.groups[i];
              break;
            }
          }
        }
        return null;
      }

      /**
       * adds new created parent to the student
       *
       */
      $scope.addParentToStudent = function (groupId, studentId, parent) {
        if ($scope.activeGroup.id === groupId) {
          $scope.activeGroup.students.forEach(
            function (student) {
              if (student.id === studentId && student.parents) {
                student.parents.push(parent);
              }
            }
          );
        }
      };


      $scope.invite = function () {
        UserService.inviteMember($scope.activeGroup.id, $scope.newStudent).then(
          function (result) {
            $scope.activeGroup.students.push(result);
            toaster.pop('info', 'Info', 'Udało się zaprosić: ' + $scope.newStudent.memberEmail);
            $scope.newStudent = {firstName: "", lastName: "", parentEmail: "", parentPhoneNumber: ""};

          }, function () {
            toaster.pop('error', 'Błąd', 'Nie udało się zaprosić: ' + $scope.newStudent.memberEmail);
            $scope.newStudent = {firstName: "", lastName: "", parentEmail: "", parentPhoneNumber: ""};
          }
        );
      };

      //$scope.$on('$viewContentLoaded', function () {
      //
      //    console.log('GroupsController init ....');
      //
      //    //fetch groups from profile
      //    $scope.groups = $rootScope.user.ownGroups;
      //    //open first group
      //    $scope.openGroup( $scope.groups[1]._id);
      //
      //    var $win = $(window);
      //
      //    var stickyMenu = $('#groups-sticky-menu');
      //    var stickyOpts = {topSpacing:70, responsiveWidth: true, getWidthFrom: '.groups-sticky-menu-width'};
      //
      //    doStickyGroupMenu();
      //
      //    $win.on('resize', function(){
      //      doStickyGroupMenu();
      //    });
      //
      //    function doStickyGroupMenu(){
      //      if( $win.width() < mq.tablet){
      //        if($scope.isMenuSticked){
      //          stickyMenu.unstick();
      //          $scope.isMenuSticked = false;
      //        }
      //      } else {
      //        if(!$scope.isMenuSticked){
      //          stickyMenu.sticky(stickyOpts);
      //          $scope.isMenuSticked = true;
      //        }
      //      }
      //    }
      //    //$scope.openGroup( $scope.groups[1]._id);
      //    //setTimeout(function(){
      //    //}, 100);
      //
      //
      //  });


      /**
       * PART OF CODE RELATED TO
       * MODAL WINDOW OF NEW GROUP CREATION
       */

      $scope.open = function (size) {

        var modalInstance = $modal.open({
          templateUrl: '/createGroup.html',
          controller: ModalInstanceCtrl,
          size: size
        });

        var state = $('#modal-state');

        modalInstance.result.then(function (newGroup) {

          UserService.createGroupNew(newGroup).then(
            function (data) {
              toaster.pop('info', 'Info', 'Udało się utworzyć klasę!');

              $rootScope.user.ownGroups.push(data);
              $scope.openGroup(data);
              //$rootScope.user.ownGroups.sort(UserService.getOwnGroupComparatorByName);
              //$scope.openGroup(newGroup._id);

            },
            function (err) {
              toaster.pop('error', 'Error', 'Nie udało się utworzyć grupy : ' + newGroup.name);
            }
          );
        }, function () {
          //do nothing - cancel has been pressed
        });
      };

      var ModalInstanceCtrl = function ($scope, $modalInstance) {

        $scope.newGroup = {
          "name": "",
          "description": ""
        }


        $scope.ok = function () {
          $modalInstance.close($scope.newGroup);

        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      };

      /**
       *  Invites parent
       *
       */

      $scope.inviteParent = function (size, groupId, studentId) {

        var modalInstance = $modal.open({
          templateUrl: '/inviteParent.html',
          controller: InviteParentCtrl,
          size: size
        });

        var state = $('#modal-state');

        modalInstance.result.then(function (parent) {
          UserService.inviteParent(groupId, studentId, parent).then(
            function (parentResponse) {
              $scope.addParentToStudent(groupId, studentId, parentResponse);
            },
            function () {
              toaster.pop('error', 'Error', 'Nie udało się zaprosić rodzica/opiekuna');
            }
          );
        }, function () {
          //do nothing - cancel has been pressed
        });
      };

      var InviteParentCtrl = function ($scope, $modalInstance) {

        $scope.newParent = {
          "parentPhoneNumber": "",
          "parentEmail": ""
        }


        $scope.ok = function () {
          $modalInstance.close($scope.newParent);

        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      };
    }]);
