function PersistingException(message) {
  this.message = message;
  this.name = "PersistingException";
}

module.exports = PersistingException;
