package com.socialshare.ui.dashboard;

import com.socialshare.custom.CustomFragment;

public abstract class TabFragment extends CustomFragment implements TabIFace {
    public final static String ATTR_TITLE = "ATTR_TITLE";
}
