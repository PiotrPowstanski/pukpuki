'use strict';
angular.module('angle').controller('OutboundMessagesController', ['$rootScope', '$window', '$scope', '$modal', 'UserService', 'toaster', '$state', function ($rootScope, $window, $scope, $modal, UserService, toaster, $state) {

  $scope.activeGroup = null;

  $scope.groups = null;

  $scope.messages = [];

  $scope.hasActiveMembers = false;

  $scope.init = function(){
    console.debug("OutboundMessagesController initialization");
    $scope.groups = $rootScope.user.ownGroups;
    if($scope.groups && $scope.groups.length > 0){
      $scope.openGroup($scope.groups[0]);
    }
  };

  $scope.openGroup = function (group) {
    $scope.activeGroup = group;
    UserService.getOutboundMessages(group._id).then(
        function(data){
          $scope.messages = data;
          //$scope.hasActiveMembers = data.hasActiveMembers;
        },
        function(err){
          //nothing to do
        }
    );
  };

  $scope.open = function (size) {

    var newMsgModalInstance = $modal.open({
      templateUrl: '/createGroup.html',
      controller: NewMsgModalCtrl,
      size: size
    });

    var state = $('#modal-state');

    newMsgModalInstance.result.then(function (newMessage) {

      UserService.createMessage($scope.activeGroup._id, newMessage).then(
        function (data) {
          $scope.messages.push(data);
        },
        function () {
          toaster.pop('error', 'Błąd', 'Nie udało się utworzyć wiadomości');
        }
      );
    }, function () {
      //do nothing - cancel has been pressed
    });
  };

  var NewMsgModalCtrl = function ($scope, $modalInstance) {

    $scope.newMessage = {
      "messageSubject": "",
      "messageText": ""
    }


    $scope.ok = function () {
      $modalInstance.close($scope.newMessage);

    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };


  $scope.checkAcceptations = function (size, students) {

    var modalInstance = $modal.open({
      templateUrl: '/checkAcceptations.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return students;
        }
      }
    });

    var state = $('#modal-state');

    modalInstance.result.then(function (newGroup) {
    }, function () {
      //do nothing - cancel has been pressed
    });
  };

  var ModalInstanceCtrl = function ($scope, $modalInstance, items) {

    $scope.items = items;

    $scope.ok = function () {
      $modalInstance.close($scope.newGroup);

    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

}]);
