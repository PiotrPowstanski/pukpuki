function InvocationException(message) {
  this.message = message;
  this.name = "InvocationException";
}

module.exports = InvocationException;
