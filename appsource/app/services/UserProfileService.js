var logger = require('winston');
var ProfileModel = require('../models/profile');
var contra = require('contra');
var InvocationException = require('../exceptions/InvocationException');
var ProfileDoesntExistException = require('../exceptions/ProfileDoesntExistException');
var GroupModel = require("../models/group");
var _ = require("underscore");
var validator = require('validator');

function UserProfileService() {

  function getProfileObject2(email, callback) {
    contra.concurrent({
        getProfile: function (cb) {
          ProfileModel.Profile.find({email: email}, {
            name: 1,
            email: 1,
            photoUrl: 1,
            _id: 0
          }, function (err, profile) {
            if (err || !profile || profile.length !== 1) {
              logger.log('error', 'Cannot find profile for email %s', email);
              cb(new ProfileDoesntExistException("Profile couldn't be found."));
            } else {
              var test = huj.huj.huj;
              cb(null, profile.shift());
            }
          });
        },
        getOwnGroups: function (cb) {
          try {
            GroupModel.Group.find(
              {ownerEmail: email},
              {name: 1, _id: 1},
              function (err, groups) {
                cb(err, groups);
              }
            );
          } catch (err) {
            cb(new InvocationException("Own groups couldn't be fetched."));
          }
        },
        getInvitedGroups: function (cb) {
          try {
            GroupModel.Group.find(
              {'members.memberEmail': email},
              {name: 1, _id: 1},
              function (err, groups) {
                cb(err, groups);
              }
            );
          } catch (err) {
            cb(new InvocationException("Invited groups couldn't be fetched."));
          }
        }
      },
      function (err, results) {
        try {
          if (err) {
            callback(null, err);
          } else {
            var profile = results.getProfile.toObject();
            var ownGroups = results.getOwnGroups;
            var invitedGroups = results.getInvitedGroups;

            if (ownGroups.length > 0) {
              profile.ownGroups = ownGroups;
            } else {
              profile.ownGroups = new Array();
            }

            if (invitedGroups.length > 0) {
              profile.invitedGroups = invitedGroups;
            } else {
              profile.invitedGroups = new Array();
            }

            callback(profile);
          }
        } catch (err) {
          callback(null, new InvocationException("Internal API exception"));
        }
      });
  }

  function getProfileObject(userEmail, userId, callback) {
    contra.concurrent({
        getProfile: function (cb) {
          ProfileModel.Profile.find({_id: userId}, {
            name: 1,
            email: 1,
            photoUrl: 1,
            _id: 0
          }, function (err, profile) {
            if (err || !profile || profile.length !== 1) {
              logger.log('error', 'Cannot find profile for email %s', userEmail);
              cb(new ProfileDoesntExistException("Profile couldn't be found."));
            } else {
              cb(null, profile.shift());
            }
          });
        },
        getOwnGroups: function (cb) {

          GroupModel.Group.find(
            {ownerProfile: userId},
            {name: 1, _id: 1},
            function (err, groups) {
              cb(err, groups);
            }
          );
        },
        getInvitedGroups: function (cb) {

          GroupModel.Group.find(
            {'members.memberEmail': userEmail},
            {name: 1, _id: 1},
            function (err, groups) {
              cb(err, groups);
            }
          );
        }
      },
      function (err, results) {

        if (err) {
          callback(null, err);
        } else {
          var profile = results.getProfile.toObject();
          var ownGroups = results.getOwnGroups;
          var invitedGroups = results.getInvitedGroups;

          if (ownGroups.length > 0) {
            profile.ownGroups = ownGroups;
          } else {
            profile.ownGroups = [];
          }

          if (invitedGroups.length > 0) {
            profile.invitedGroups = invitedGroups;
          } else {
            profile.invitedGroups = [];
          }

          callback(profile);
        }

      });
  }

  function getProfileEmail(email, callback) {
    try {
      ProfileModel.Profile.find(
        {email: email},
        {name: 1},
        function (err, profile) {
          if (!err && profile && profile.length === 1) {
            callback(profile.shift().name);
          } else {
            callback(null, ProfileDoesntExistException("Profile for given user couldn't be found"));
          }
        }
      );
    } catch (err) {
      callback(null, InvocationException("Name for given user couldn't be fetched"));
    }
  }

  return {
    getProfileEmail: getProfileEmail,
    getProfileObject: getProfileObject,
    getProfileObject2: getProfileObject2
  }
};

module.exports = new UserProfileService();
