function MessageNotFoundException(message) {
  this.message = message;
  this.name = "MessageNotFoundException";
}

module.exports = MessageNotFoundException;
