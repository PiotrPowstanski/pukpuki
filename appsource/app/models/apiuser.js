var mongoose = require('mongoose');

var apiUserSchema = mongoose.Schema({
    email        : {type: String, required: true},
    name         : {type: String, required: false},
    currentAuth  : {type: String, required: true, enum: ['FACEBOOK', 'GOOGLE', 'BEARER', 'LOCAL']},
    currentAuthId: {type: String},
    idleTimeMin  : {type: Number, default : 30},
    lastCallTime : {type: Date, default: Date.now},
    creationTime : {type: Date, default: Date.now},
    profile: {type: mongoose.Schema.ObjectId, ref:'Profile', required: false}
  });

// create the model for users and expose it to our app
module.exports = {
    ApiUser : mongoose.model('ApiUser', apiUserSchema)
}
