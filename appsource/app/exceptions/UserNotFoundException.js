function UserNotFoundException(message) {
    this.message = message;
    this.name = "UserNotFoundException";
}

module.exports = UserNotFoundException;
