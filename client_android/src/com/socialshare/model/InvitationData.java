package com.socialshare.model;

public class InvitationData {

    private String groupName;

    private String schoolName;

    private String student;

    private boolean isAccepted;

    private int profileImage;

    private String invitationDate;

    public InvitationData(String groupName, String schoolName, String student, String invitationDate, boolean isAccepted, int profileImage) {
        this.groupName = groupName;
        this.schoolName = schoolName;
        this.student = student;
        this.isAccepted = isAccepted;
        this.profileImage = profileImage;
        this.invitationDate = invitationDate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public int getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(int profileImage) {
        this.profileImage = profileImage;
    }

    public String getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(String invitationDate) {
        this.invitationDate = invitationDate;
    }
}
