// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var GoogleOneTimeAuthCodeStrategy = require('passport-google-authcode').Strategy;
var GoogleIDTokenStrategy = require('passport-google-id-token').Strategy;

// load up the user model
var ApiUserSessionModel = require('../app/models/apiuser');
var ProfileModel = require('../app/models/profile');

var ObjectId = require('mongoose').Types.ObjectId;

var contra = require('contra');

// load the auth variables
var configAuth = require('./auth');

// oAuth2 request
var request = require('request');
var options = {
    host: 'https://www.googleapis.com',
    userInfoPath: '/oauth2/v1/userinfo?alt=json&access_token=',
    tokenInfoPath: '/oauth2/v1/tokeninfo?access_token=',
    accessCodeExchangePath: '/oauth2/v3/token'
};

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        ApiUserSessionModel.ApiUser.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    //passport.use('local-login', new LocalStrategy({
    //    // by default, local strategy uses username and password, we will override with email
    //    usernameField : 'email',
    //    passwordField : 'password',
    //    passReqToCallback : true // allows us to pass back the entire request to the callback
    //},
    //function(req, email, password, done) { // callback with email and password from our form
    //
    //    // find a user whose email is the same as the forms email
    //    // we are checking to see if the user trying to login already exists
    //    User.findOne({ 'local.email' :  email }, function(err, user) {
    //        // if there are any errors, return the error before anything else
    //        if (err)
    //            return done(err);
    //
    //        // if no user is found, return the message
    //        if (!user)
    //            return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
    //
    //        // if the user is found but the password is wrong
    //        if (!user.validPassword(password))
    //            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
    //
    //        // all is well, return successful user
    //        return done(null, user);
    //    });
    //
    //}));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL

    },
    function(token, refreshToken, fbprofile, done) {
        // asynchronous
        process.nextTick(function() {

          process.nextTick(function () {
            ProfileModel.Profile.findOrCreate({email: fbprofile.emails[0].value}, {
              name: fbprofile.name.givenName + ' ' + fbprofile.name.familyName
            }, function (err, profile) {
              if (!err && profile) {
                var newApiUserSession = new ApiUserSessionModel.ApiUser();
                newApiUserSession.email = fbprofile.emails[0].value;
                newApiUserSession.name = fbprofile.name.givenName + ' ' + fbprofile.name.familyName;
                newApiUserSession.currentAuth = 'FACEBOOK';
                //newApiUserSession.currentAuthId = googleProfile.id;
                newApiUserSession.profile = profile._id;
                newApiUserSession.save(function (err) {
                  if (err) {
                    return done(err);
                  } else {
                    return done(null, newApiUserSession);
                  }
                });
              } else {
                return done(err);
              }
            });
          });


          var newApiUserSession = new ApiUserSessionModel.ApiUser();
          newApiUserSession.email = fbprofile.emails[0].value;
          newApiUserSession.name = fbprofile.name.givenName + ' ' + fbprofile.name.familyName;
          newApiUserSession.currentAuth = 'FACEBOOK';
          newApiUserSession.profile = new ObjectId('54bbc375c0f53d460c323d62');
          newApiUserSession.save(function(err){
            if(err)
              return done(err);
          });

          //ProfileModel.Profile.findOne({email : newApiUserSession.email}, {email: 1, name: 1, _id:0}, function(err, profile){
          //  if(err){
          //    newApiUserSession.delete();
          //    return done(err);
          //  }
          //
          //  if(profile){
          //    return done(null, newApiUserSession);
          //  } else {
          //    var newProfile = new ProfileModel.Profile();
          //    newProfile.email = newApiUserSession.email;
          //    newProfile.name = newApiUserSession.name;
          //    newProfile.save(function(err){
          //      if(err){
          //        newApiUserSession.delete();
          //        return done(err);
          //      } else {
          //        return done(null, newApiUserSession);
          //      }
          //    });
          //
          //  }
          //});
        });

    }
    ));


  /**
   * Google authentication
   *
   */
  passport.use(new GoogleStrategy({
      clientID: configAuth.googleAuth.clientID,
      clientSecret: configAuth.googleAuth.clientSecret,
      callbackURL: configAuth.googleAuth.callbackURL
    },
    function (token, refreshToken, googleProfile, done) {
      process.nextTick(function () {
        ProfileModel.Profile.findOrCreate({email: googleProfile.emails[0].value}, {
          name: googleProfile.displayName
        }, function (err, profile) {
          if (!err && profile) {
            var newApiUserSession = new ApiUserSessionModel.ApiUser();
            newApiUserSession.email = googleProfile.emails[0].value;
            newApiUserSession.name = googleProfile.displayName;
            newApiUserSession.currentAuth = 'GOOGLE';
            newApiUserSession.currentAuthId = googleProfile.id;
            newApiUserSession.profile = profile._id;
            newApiUserSession.save(function (err) {
              if (err) {
                return done(err);
              } else {
                return done(null, newApiUserSession);
              }
            });
          } else {
            return done(err);
          }
        });
      });

    }
  ));

    passport.use(new GoogleOneTimeAuthCodeStrategy({
            clientID: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret
        },
        function(accessToken, refreshToken, profile, done) {
            console.log('[8] GoogleOneTimeAuthCodeStrategy');

            var profileEmail = profile.emails[0].value;

            ProfileModel.Profile.findOrCreate({ email: profileEmail }, function (err, user) {
                console.log("findOrCreate");
                return done(err, user);
            });
        }
    ));

    passport.use(new GoogleIDTokenStrategy({
            clientID: configAuth.googleAuth.clientID
            //getGoogleCerts: customGetGoogleCerts
        },
        function(parsedToken, googleId, done) {
            console.log('[9] GoogleIDTokenStrategy');

            var profileEmail = parsedToken.data.email;
            var idToken = parsedToken.data;
            //iss: always accounts.google.com
            //aud: the client ID of the web component of the project
            //azp: the client ID of the Android app component of project
            //email: the email that identifies the user requesting the token
            //console.log(idToken);

            ProfileModel.Profile.findOrCreate({ email: profileEmail }, function (err, user) {
                return done(err, user);
            });
        }
    ));

    // =========================================================================
    // oAuth2 ==================================================================
    // =========================================================================

    callback = function(response) {
        console.log('Callback - start');
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            console.log(str);
        });
    }

    // BearerStrategy - start
    passport.use(new BearerStrategy(
        function(token, done) {

            var verifyTokenURL = options.host + options.tokenInfoPath + token;
            var verifyUserURL = options.host + options.userInfoPath + token;
            console.log('access_token=' + token);

            contra.waterfall([
                // verify token - start
                function (next) {
                    console.log('[1] - verifyTokenURL started ...');
                    var isOK = false;

                    // verify token - check appID
                    console.log('Making request to: ' + verifyTokenURL);
                    request(verifyTokenURL, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log(body); // Print the google response.

                            // check if the token is issued to this application
                            var jsonResponse = JSON.parse(body);
                            var androidAppID = configAuth.androidAuth.clientID;
                            if(jsonResponse.issued_to !== androidAppID) {
                                console.log('Wrong token!');
                                isOK = false;
                                next(null, isOK);
                                //return done(err, { message: 'Wrong token!' });
                            } else {
                                var tokenExpirationTime = Math.floor(jsonResponse.expires_in / 60);
                                console.log('Token verification PASSED.');
                                console.log('Token will expire in ' + tokenExpirationTime + ' minutes.');
                                isOK = true;
                                next(null, isOK);
                            }
                        } else {
                            console.log('verifyTokenURL Request failed!');
                            console.log(body);
                            isOK = false;
                            next(null, isOK);
                            //return done(error);
                        }
                    });

                    //next(null, 'params for', 'next', 'step');

                }, // verify token - end

                // verify user - start
                function (isTokenOK, next) {
                    console.log('[2] - verifyUserURL started ...');
                    var isOK = false;

                    if (!isTokenOK) {
                        isOK = false;
                        next(null, isOK, '');
                    } else {
                        // verify user and create an account if needed
                        console.log('Making request to: ' + verifyUserURL);
                        request(verifyUserURL, function (error, response, body) {
                            if (!error && response.statusCode == 200) {
                                isOK = true;
                                console.log(body); // Print the google response.
                                var jsonResponse = JSON.parse(body);
                                next(null, isOK, jsonResponse);
                            } else {
                                isOK = false;
                                console.log('verifyUserURL Request failed!');
                                console.log(body);
                                next(null, isOK, '');
                                //return done(error);
                            }
                        })
                    }
                 }, // verify user - end

                // create user if needed - start
                function (responseOK, jsonResponse, next) {
                    console.log('[3] - create user if needed started ...');
                    var isOK = false;

                    if(!responseOK) {
                        next(null, responseOK, 'Failed at step [3] - response not OK!');
                    } else {
                        // Query for the user using Google e-mail
                        ProfileModel.Profile.findOne({'email': jsonResponse.email }, function (err, profile) {
                            if (err) {
                                console.log('!Error on BearerStrategy/ProfileModel.Profile.findOne. DB exception');
                                //return done(err, { message: 'Error on BearerStrategy/ProfileModel.Profile.findOne. DB exception!' });
                                isOK = false;
                                next(null, isOK, 'Failed at step [3] - Error on BearerStrategy/ProfileModel.Profile.findOne. DB exception');
                            }
                            if (!profile) {
                                console.log('No such user profile found. Creating a new profile.');
                                var newProfile = new ProfileModel.Profile();
                                newProfile.email = jsonResponse.email;
                                newProfile.name = jsonResponse.name;
                                //TODO: Currently overwritten by model "trigger"
                                newProfile.photoUrl = jsonResponse.picture;
                                newProfile.save(function(err) {
                                    if(err) {
                                        console.log('Cannot create a new profile!');
                                        isOK = false;
                                        next(null, isOK, '[3] Cannot create a new profile!');
                                        //return done(err, { message: 'Cannot create a new profile!' });
                                    } else {
                                        console.log('User has been created.');
                                        isOK = true;
                                        next(null, isOK, newProfile); // newProfile here!!!
                                        //return done(null, newProfile, { scope: 'read' });
                                    }
                                });
                            } else {
                                console.log('User has been found.');
                                isOK = true;
                                next(null, isOK, profile);
                                //return done(null, profile, { scope: 'read' });
                            }
                        });
                    }

                } // create user if needed - end

            // return http response - start
            ], function (err, ok, result) {
                console.log('[4] - return http response started...');
                if(!ok) {
                    console.log("[4] Error: " + result);
                    return done(err);
                } else {
                    console.log('[4] ALL OK');
                    return done(null, result);
                }
            });
            // return http response - end
        }
    )); // BearerStrategy - end

};
