var ValidationException = require("../exceptions/ValidationException");
var validator = require('validator');

function ValidationService() {

  function validateEmail(email, callback) {
    try {
      if (!validator.isEmail(email)) {
        callback(false, new ValidationException("Given email is not valid"));
      } else {
        callback(true);
      }
    } catch (err) {
      callback(false, new ValidationException("Given email is not valid"));
    }
  }

  return {
    validateEmail: validateEmail
  }
}

module.exports = new ValidationService();
