package com.socialshare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;

/**
 * The Class Explore is the Fragment class that is launched when the user clicks
 * on Explore in Left navigation drawer.
 */
public class Explore extends CustomFragment
{

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.explore, null);

		return v;
	}

}
