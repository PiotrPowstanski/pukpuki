var Student = require('../response/StudentResponse');
var _ = require("underscore");

function OutboundMessage(students, message) {
  this.id = message._id;
  this.groupId = message._groupId;
  this.subject = message.messageSubject;
  this.text = message.messageText;
  this.creationDate = message.creationDate;
  this.students = [];
  this.addStudents(students);
  this.extendByAcceptation(message.recipients);
}

OutboundMessage.prototype.addStudents = function (students) {
  var that = this;
  students.forEach(function (student) {
    that.students.push(new Student(student));
  });
};

OutboundMessage.prototype.extendByAcceptation = function (recipients) {
  var that = this;
  that.students.forEach(function (student) {
    student.parents.forEach(function(parent){
      var parentRecipient = _.find(recipients, function(recipient){
        return recipient.parent.equals(parent.id);
      });
      parent.extendByConfirmation(parentRecipient.confirmationDate);
    })
  });
};

module.exports = OutboundMessage;
