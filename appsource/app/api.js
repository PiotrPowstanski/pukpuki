var logger = require('winston');
var passportSocketIo = require("passport.socketio");
var Message = require('../app/models/message');
var UserProfileService = new require('../app/services/UserProfileService');
var MessagesService = new require('../app/services/MessagesService');
var ApiUserService = new require('../app/services/ApiUserService');
var GroupService = new require('../app/services/GroupService');
var InvitationService = new require('../app/services/InvitationService');
var ResponseService = require('../app/services/ResponseService');
var EventService =  require('../app/services/EventService');
var EventCrator = require("../app/common/EventCreator");

module.exports = function (app, express) {

  /**
   * gets profile
   *
   */
  app.get('/api/parents/:id', isLoggedIn, function (req, resp) {
    var email = req.user.email;
    var parentId = req.params.id;
    GroupService.getParent(parentId, function (parent, err) {
      var responseService = new ResponseService(resp);
      if (err) {
        responseService.sendError(err);
      } else {
        responseService.send(parent);
      }
    });
  });

    /**
     * gets profile
     *
     */
    app.get('/api/profile', isLoggedIn, function (req, resp) {
      var userEmail = req.user.email;
      var userId = req.user.profile;
        UserProfileService.getProfileObject(userEmail, userId, function (userProfile, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(userProfile);
            }
        });
    });

    /**
     * creates new group
     *
     */
    app.post('/api/groups', isLoggedIn, function (req, resp) {
        var userEmail = req.user.email;
        var userId = req.user.profile;
        var group = req.body;
        GroupService.createGroup(userEmail, userId, group, function (result, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(result);
            }
        });
    });

  app.post('/api/groups/:groupId/students/:studentId/parents', isLoggedIn, function (req, resp) {
    var email = req.user.email;
    var groupId = req.params.groupId;
    var studentId = req.params.studentId;
    var parent = req.body;
    InvitationService.inviteParent(email, groupId, studentId, parent, function (result, err) {
      var responseService = new ResponseService(resp);
      if (err) {
        responseService.sendError(err);
      } else {
        responseService.send(result);
      }
    });
  });

    ///**
    // * gets invitations
    // *
    // */
    //app.get('/api/invitations', isLoggedIn, function (req, resp) {
    //    var email = req.user.email;
    //    InvitationService.getInvitationsByEmail(email, function (result, err) {
    //        var responseService = new ResponseService(resp);
    //        if (err) {
    //            responseService.sendError(err);
    //        } else {
    //            responseService.send(result);
    //        }
    //    })
    //});

  app.get('/api/invitations', isLoggedIn, function (req, resp) {
    var userEmail = req.user.email;
    var userProfile = req.user.profile;

    InvitationService.getInvitations(userEmail, userProfile, function (result, err) {
      var responseService = new ResponseService(resp);
      if (err) {
        responseService.sendError(err);
      } else {
        responseService.send(result);
      }
    })
  });

    app.get('/api/invitation/:id', isLoggedIn, function (req, resp) {
        var email = req.user.email;
        var memberId = req.params.id;
        InvitationService.getInvitationsByEmail(email, memberId, function (result, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(result);
            }
        })
    });
  /**
   * accepts invitation
   *
   */
  app.get('/api/invitations/:groupId/:studentId/accept', isLoggedIn, function (req, resp) {
    var email = req.user.email;
    var userProfile = req.user.profile;
    var groupId = req.params.groupId;
    var studentId = req.params.studentId;
    InvitationService.acceptOrDeclineInvitation(email, userProfile, groupId, studentId, 'accept', function (result, err) {
      var responseService = new ResponseService(resp);
      if (err) {
        responseService.sendError(err);
      } else {
        responseService.send(result);
        //responseService.send(result.response);
        //var data = result.event;
        //new EventService(app,req).broadcast(data.ownerEmail, EventCrator.events.INVITATION_ACCEPTED_OR_DECLINED, data);
      }
    });

  });

  /**
   * declines invitation
   *
   */
  app.get('/api/invitations/:groupId/:studentId/decline', isLoggedIn, function (req, resp) {
    var email = req.user.email;
    var userProfile = req.user.profile;
    var groupId = req.params.groupId;
    var studentId = req.params.studentId;
    InvitationService.acceptOrDeclineInvitation(email, userProfile, groupId, studentId, 'decline', function (result, err) {
      var responseService = new ResponseService(resp);
      if (err) {
        responseService.sendError(err);
      } else {
        responseService.send(result);
        //responseService.send(result.response);
        //var data = result.event;
        //new EventService(app,req).broadcast(data.ownerEmail, EventCrator.events.INVITATION_ACCEPTED_OR_DECLINED, data);
      }
    })
  });

    /**
     * gets particular groups details
     *
     */
    app.get('/api/groups/:id', isLoggedIn, function (req, resp) {
        var email = req.user.email;
        var groupId = req.params.id;
        GroupService.getGroup(email, groupId, function (group, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(group);
            }
        });
    });

    /**
     * creates new member for particular group
     *
     */
    app.post('/api/groups/:id/invite', isLoggedIn, function (req, resp) {
        var email = req.user.email;
        var groupId = req.params.id;
        var member = req.body;
        GroupService.inviteMember(email, groupId, member, function (member, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(member);
                var data = {};
                data.id = member._id;
                data.groupName =  member.groupName;
                data.schoolName =  member.schoolName;
                new EventService(app,req).broadcast(member.memberEmail, EventCrator.events.INVITATION, data);
            }
        });
    });

    /**
     * creates new message
     *
     */
    app.post('/api/messages', isLoggedIn, function (req, resp) {
        var userEmail = req.user.email;
        var userProfileId = req.user.profile;
        var message = req.body.message;
        var groupId = req.body.groupId;

        MessagesService.createMessage(userEmail, userProfileId, groupId, message, function (newMessage, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(newMessage);
            }
        })
    });

    /**
     * gets outbound messages
     *
     */
    app.get('/api/groups/:groupId/messages/outbound', isLoggedIn, function (req, resp) {
      var userProfile = req.user.profile;
      var groupId = req.params.groupId;
        MessagesService.getOutboundMessages(userProfile, groupId, function (messages, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(messages);
            }
        })
    });

    /**
     * confirms particular message
     *
     */
    app.get('/api/inboundmessages/:messageId/confirm/:recipientId', isLoggedIn, function (req, resp) {
        var email = req.user.email;
        var messageId = req.params.messageId;
        var recipientId = req.params.recipientId;
        MessagesService.confirmReading(email, messageId, recipientId,function (result, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(result);
            }
        })
    });

    app.get('/api/inboundmessages', isLoggedIn, function (req, resp) {
      var userProfileId = req.user.profile;
      var period = req.params.period;
        MessagesService.getInboundMessages(userProfileId, period, function (messages, err) {
            var responseService = new ResponseService(resp);
            if (err) {
                responseService.sendError(err);
            } else {
                responseService.send(messages);
            }
        })
    });

    app.io.route('ready', function(req) {
        if(req.session && req.session.passport){
            var userId = req.session.passport.user;
            if(userId){
                ApiUserService.getEmail(userId, function(email){
                    if(email){
                        req.io.join(email);
                    }
                });
            }
        }

    });
};

// route middleware to make sure
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't send them 401 to describe what happened
    res.status('401').send('HTTP Error 401 Unauthorized');
}
