function ProfileDoesntExistException(message) {
  this.message = message;
  this.name = "ProfileDoesntExistException";
}

module.exports = ProfileDoesntExistException;

