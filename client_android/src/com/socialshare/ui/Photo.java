package com.socialshare.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;
import com.socialshare.model.Data;

/**
 * The Class Photo is the Fragment class that is launched when the user
 * clicks on News feed option in Left navigation drawer.
 * This Fragment simply shows a Photo and display a dummy list of Comments on the photo.
 * You need to write the actual implementation for loading and displaying photo and comments.
 */
public class Photo extends CustomFragment
{

	/** The list of comments. */
	private ArrayList<Data> cList;

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.photo, null);

		loadComments();
		ListView list = (ListView) v.findViewById(R.id.list);
		list.setAdapter(new CutsomAdapter());

		setTouchNClick(v.findViewById(R.id.btnLike));
		setTouchNClick(v.findViewById(R.id.btnComment));
		return v;
	}

	/**
	 * This method currently loads a dummy list of comments. You can write the
	 * actual implementation of loading comments.
	 */
	private void loadComments()
	{
		cList = new ArrayList<Data>();
		cList.add(new Data("Svetin", "Ooo yeah!", R.drawable.user1));
		cList.add(new Data("Renu", "It's awesome!!!!", R.drawable.user2));
		cList.add(new Data("Santa", "Love it, Ice is looking great..",
				R.drawable.user3));
		cList.add(new Data("Steve", "Nice one", R.drawable.user4));
		cList.add(new Data("Peter", "waw...looking cool", R.drawable.user5));
		cList.add(new Data("Svetin", "Ooo yeah!", R.drawable.user1));
		cList.add(new Data("Renu", "It's awesome!!!!", R.drawable.user2));
		cList.add(new Data("Santa", "Love it, Ice is looking great..",
				R.drawable.user3));
		cList.add(new Data("Steve", "Nice one", R.drawable.user4));
		cList.add(new Data("Peter", "waw...looking cool", R.drawable.user5));
	}

	/**
	 * The Class CutsomAdapter is the adapter class for comment ListView. The
	 * currently implementation of this adapter simply display static dummy
	 * list of Comments. You need to write the code for loading actual comments.
	 */
	private class CutsomAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return cList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Data getItem(int arg0)
		{
			return cList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.comment_item, null);

			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(getItem(pos).getTitle1());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(getItem(pos).getDesc());

			ImageView img = (ImageView) v.findViewById(R.id.img1);
			img.setImageResource(getItem(pos).getImage1());
			return v;
		}

	}
}
