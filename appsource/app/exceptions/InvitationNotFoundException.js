function InvitationNotFoundException(message) {
  this.message = message;
  this.name = "InvitationNotFoundException";
}

module.exports = InvitationNotFoundException;
