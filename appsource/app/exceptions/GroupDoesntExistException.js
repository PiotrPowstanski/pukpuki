function GroupDoesntExistException(message) {
  this.message = message;
  this.name = "GroupDoesntExistException";
}

module.exports = GroupDoesntExistException;

