package com.socialshare.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;
import com.socialshare.model.Data;

/**
 * The Class ActivityList is the Fragment class that is launched when the user
 * clicks on Activity option in Left navigation drawer.
 */
public class ActivityList extends CustomFragment
{

	/** The Activity list. */
	private ArrayList<Data> actList;

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.activity_list, null);

		loadActivities();
		ListView list = (ListView) v.findViewById(R.id.list);
		list.setAdapter(new CutsomAdapter());

		setTouchNClick(v.findViewById(R.id.tab1));
		setTouchNClick(v.findViewById(R.id.tab2));
		return v;
	}

	/* (non-Javadoc)
	 * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.tab1)
		{
			getView().findViewById(R.id.tab2).setEnabled(true);
			v.setEnabled(false);
		}
		else if (v.getId() == R.id.tab2)
		{
			getView().findViewById(R.id.tab1).setEnabled(true);
			v.setEnabled(false);
		}
	}

	/**
	 * This method currently loads a dummy list of activities. You can write the
	 * actual implementation of loading categories.
	 */
	private void loadActivities()
	{
		actList = new ArrayList<Data>();
		actList.add(new Data("Svetin", "liked your photo", "1 hour ago",
				R.drawable.user1, R.drawable.animal1));
		actList.add(new Data("Renu", "commented on your photo", "2 hours ago",
				R.drawable.user2, R.drawable.animal2));
		actList.add(new Data("Santa", "liked your photo", "1 day ago",
				R.drawable.user3, R.drawable.animal3));
		actList.add(new Data("Steve", "liked your video", "2 weeks ago",
				R.drawable.user4, R.drawable.animal4));
		actList.add(new Data("Peter", "commented on your video", "1 month ago",
				R.drawable.user5, R.drawable.animal5));
		actList.add(new Data("Svetin", "liked your photo", "1 month ago",
				R.drawable.user1, R.drawable.animal6));
		actList.add(new Data("Renu", "commented on your photo", "2 months ago",
				R.drawable.user2, R.drawable.animal7));
		actList.add(new Data("Santa", "liked your photo", "2 months ago",
				R.drawable.user3, R.drawable.animal8));
		actList.add(new Data("Steve", "liked your video", "2 months ago",
				R.drawable.user4, R.drawable.animal4));
		actList.add(new Data("Peter", "commented on your video",
				"2 months ago", R.drawable.user5, R.drawable.animal5));
	}

	/**
	 * The Class CutsomAdapter is the adapter class for Activity ListView. The
	 * currently implementation of this adapter simply display static dummy
	 * contents. You need to write the code for displaying actual contents.
	 */
	private class CutsomAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return actList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Data getItem(int arg0)
		{
			return actList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.activity_item, null);

			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(getItem(pos).getTitle1());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(getItem(pos).getTitle2());

			lbl = (TextView) v.findViewById(R.id.lbl3);
			lbl.setText(getItem(pos).getDesc());

			ImageView img = (ImageView) v.findViewById(R.id.img1);
			img.setImageResource(getItem(pos).getImage1());

//			img = (ImageView) v.findViewById(R.id.img2);
//			img.setImageResource(getItem(pos).getImage2());
			return v;
		}

	}
}
