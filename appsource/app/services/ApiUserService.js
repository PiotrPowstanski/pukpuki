var ObjectId = require('mongoose').Types.ObjectId;
var ApiUserModel = require("../models/apiuser");

function ApiUserService() {
    function getEmail(userId, callback) {
        try {
            ApiUserModel.ApiUser.findOne(
                {_id: new ObjectId(userId)},
                {email:1},
                function (err, user) {
                    try {
                        if(err || !user){
                            callback(null);
                        } else {
                            callback(user.email);
                        }
                    } catch (err) {
                        callback(null);
                    }
                }
            );
        } catch (err) {
            callback(null);
        }
    }

    return {
        getEmail:getEmail
    }
}

module.exports = new ApiUserService();
