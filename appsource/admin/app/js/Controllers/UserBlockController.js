'use strict';
angular.module('angle').controller('UserBlockController', ['$rootScope', '$window', '$scope', 'UserService', function($rootScope, $window, $scope, UserService) {

  $scope.init = function(){
    console.debug("UserBlockController initialization");
    $scope.profile = $rootScope.user;
  }

  $scope.profile = null;

  $scope.userBlockVisible = true;

  $scope.$on('toggleUserBlock', function(event, args) {
    $scope.userBlockVisible = ! $scope.userBlockVisible;
  });

}]);
