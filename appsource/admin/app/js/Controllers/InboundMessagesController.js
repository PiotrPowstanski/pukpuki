'use strict';
angular.module('angle').controller('InboundMessagesController', ['$rootScope', '$window', '$scope', '$modal', 'UserService', 'toaster', '$state', function ($rootScope, $window, $scope, $modal, UserService, toaster, $state) {

  $scope.messages = null;

  $scope.init = function(){
    console.debug("InboundMessagesController initialization");

    UserService.getInboundMessages().then(
        function (data) {
          $scope.messages = data;
        },
        function (err) {
          //nothing to do
        }
    );
  }

  $scope.confirmReading = function(message){
      UserService.confirmReading(message.id, message.recipientId).then(
          function () {
              message.confirmed = true;
              toaster.pop('info', 'Info', 'Udało się potwierdzic wiadomosc');
          },
          function () {
              message.confirmed = false;
              toaster.pop('error', 'Błąd', 'Nie udało się zaakceptować wiadomosci');
          }
      );
  }

}]);