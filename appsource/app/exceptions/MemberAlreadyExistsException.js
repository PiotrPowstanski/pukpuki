function MemberAlreadyExistsException(message) {
  this.message = message;
  this.name = "MemberAlreadyExistsException";
}

module.exports = MemberAlreadyExistsException;
