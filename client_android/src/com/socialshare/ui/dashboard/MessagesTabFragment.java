package com.socialshare.ui.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.model.Data;
import com.socialshare.ui.VerticalViewPager;

import java.util.ArrayList;
import java.util.Locale;

public class MessagesTabFragment extends TabFragment {

   // private VerticalViewPager viewPager;
   private ArrayList<Data> actList;

    public static MessagesTabFragment instantiate(String tabTitle){
        Bundle bundle =  new Bundle();
        bundle.putString(ATTR_TITLE, tabTitle);
        MessagesTabFragment planTF = new MessagesTabFragment();
        planTF.setArguments(bundle);

        return planTF;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.plan, container, false);
        loadActivities();
        ListView list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(new CutsomAdapter());
     //   viewPager = (VerticalViewPager) view.findViewById(R.id.scheduleViewPager);
     //   viewPager.setAdapter(new DummyAdapter(getFragmentManager()));
        return view;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(ATTR_TITLE);
    }

    public class DummyAdapter extends FragmentStatePagerAdapter {

        public DummyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "PAGE 1";
                case 1:
                    return "PAGE 2";
                case 2:
                    return "PAGE 3";
            }
            return null;
        }

    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            TextView rootView = new TextView(inflater.getContext());
            rootView.setText(R.string.app_name);
            return rootView;
        }


    }

    private class CutsomAdapter extends BaseAdapter
    {

        /* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
        @Override
        public int getCount()
        {
            return actList.size();
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
        @Override
        public Data getItem(int arg0)
        {
            return actList.get(arg0);
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
        @Override
        public long getItemId(int arg0)
        {
            return arg0;
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        @Override
        public View getView(int pos, View v, ViewGroup arg2)
        {
            if (v == null)
                v = LayoutInflater.from(getActivity()).inflate(
                        R.layout.activity_item, null);

            TextView lbl = (TextView) v.findViewById(R.id.lbl1);
            lbl.setText(getItem(pos).getTitle1());

            lbl = (TextView) v.findViewById(R.id.lbl2);
            lbl.setText(getItem(pos).getTitle2());

            lbl = (TextView) v.findViewById(R.id.lbl3);
            lbl.setText(getItem(pos).getDesc());

            ImageView img = (ImageView) v.findViewById(R.id.img1);
            img.setImageResource(getItem(pos).getImage1());

//            img = (ImageView) v.findViewById(R.id.img2);
//            img.setImageResource(getItem(pos).getImage2());
            return v;
        }

    }

    private void loadActivities()
    {
        actList = new ArrayList<Data>();
        actList.add(new Data("Spotkanie z rodzicami 12.12.2014", "Klasa 1a", "10.12.2014",
                R.drawable.user1, R.drawable.animal1));
        actList.add(new Data("Wywiadówka", "Klasa 1a", "2 hours ago",
                R.drawable.user2, R.drawable.animal2));
        actList.add(new Data("Wycieczka do ZOO", "Klasa 1a", "1 day ago",
                R.drawable.user3, R.drawable.animal3));
        actList.add(new Data("Pozwolenie na prowadzenie bloga", "Klasa 2a", "2 weeks ago",
                R.drawable.user4, R.drawable.animal4));
        actList.add(new Data("Potrzebne farby na plastykę", "Klasa 1a", "1 month ago",
                R.drawable.user5, R.drawable.animal5));
        actList.add(new Data("Wybory samorządu klasy", "Klasa 1a", "1 month ago",
                R.drawable.user1, R.drawable.animal6));
        actList.add(new Data("Odwołane zajęcie z niemieckiego", "Klasa 1a", "2 months ago",
                R.drawable.user2, R.drawable.animal7));
        actList.add(new Data("Wybory samorządu klasy", "Klasa 1a", "2 months ago",
                R.drawable.user3, R.drawable.animal8));
        actList.add(new Data("Potrzebne farby na plastykę", "Klasa 1a", "2 months ago",
                R.drawable.user4, R.drawable.animal4));
        actList.add(new Data("Potrzebne farby na plastykę", "Klasa 1a",
                "2 months ago", R.drawable.user5, R.drawable.animal5));
    }
}
