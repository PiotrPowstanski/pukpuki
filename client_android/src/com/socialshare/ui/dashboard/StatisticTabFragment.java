package com.socialshare.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialshare.R;

public class StatisticTabFragment extends TabFragment {

    public static StatisticTabFragment instantiate(String tabTitle){
        Bundle bundle =  new Bundle();
        bundle.putString(ATTR_TITLE, tabTitle);
        StatisticTabFragment statTF = new StatisticTabFragment();
        statTF.setArguments(bundle);
        return statTF;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View statView = inflater.inflate(R.layout.statistics, container, false);
        return statView;
    }

    @Override
    public String getTitle() {
        return getArguments().getString(ATTR_TITLE);
    }
}
