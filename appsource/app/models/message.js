var paginate = require('mongoose-paginate');
var mongoose = require('mongoose');

var messageRecipientSchema = mongoose.Schema({
  parent : {type: mongoose.Schema.ObjectId, required: true},
  student : {type: mongoose.Schema.ObjectId, required: true},
  profile : {type: mongoose.Schema.ObjectId, required: true},
  confirmationDate : {type: Date, default:null}
});

var messageSchema = mongoose.Schema({
  group: {type: mongoose.Schema.ObjectId, ref: 'Group', required: true},
  owner: {type: mongoose.Schema.ObjectId, ref: 'Profile', required: true},
  messageSubject: {type: String, required: true},
  messageText: {type: String, required: true},
  creationDate: {type: Date, default: Date.now},
  recipients: [messageRecipientSchema]
});

messageSchema.plugin(paginate);

module.exports = {
  MessageRecipient : mongoose.model('MessageRecipient', messageRecipientSchema),
  Message : mongoose.model('Message', messageSchema)
}
