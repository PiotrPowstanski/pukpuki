var mongoose = require('mongoose');
var _ = require("underscore");

var parentSchema = mongoose.Schema({
  parentEmail : {type: String},
  invitationAccepted: {type: Boolean, default: false, require: true},
  invitationDate: {type: Date, default: Date.now},
  parentPhoneNumber: {type: String},
  profile: {type: mongoose.Schema.ObjectId, ref:'Profile'}
});

var studentSchema = mongoose.Schema({
  firstName : {type: String, required: true},
  lastName : {type: String, required: true},
  parents : [parentSchema]
});

var groupSchema = mongoose.Schema({
  name: {type: String, required: true},
  ownerEmail: {type: String, required: true, index : true},
  ownerProfile: {type: mongoose.Schema.ObjectId, ref:'Profile', required: true},
  description : {type: String, required: true},
  creationDate: {type: Date, default: Date.now},
  students: [studentSchema]
});

module.exports = {
    Group: mongoose.model('Group', groupSchema),
    Student: mongoose.model('Student', studentSchema),
    Parent: mongoose.model('ParentResponse', parentSchema)
};

