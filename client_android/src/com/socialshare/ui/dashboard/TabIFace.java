package com.socialshare.ui.dashboard;

/**
 * Created by sunteam on 20.03.2014.
 */
public interface TabIFace {
    String getTitle();
}
