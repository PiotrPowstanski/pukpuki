function InvalidParameterException(message) {
  this.message = message;
  this.name = "InvalidParameterException";
}

module.exports = InvalidParameterException;
