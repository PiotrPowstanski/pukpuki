package com.socialshare.ui.dashboard;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.socialshare.R;
import com.socialshare.custom.CustomFragment;

import java.util.ArrayList;
import java.util.List;

public class Dashboard extends CustomFragment implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    private FragmentTabHost tabHost;
    private ViewPager viewPager;
    private List<TabFragment> tabFragments;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.dashtab, container, false);

        FragmentManager fm = getActivity().getSupportFragmentManager();

        tabFragments = initDashboardFragements();

        DashboardPagerAdapter dashTabPageAdapter = new DashboardPagerAdapter(fm, tabFragments);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

        viewPager.setAdapter(dashTabPageAdapter);

        viewPager.setOnPageChangeListener(this);


        tabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);

        tabHost.setOnTabChangedListener(this);

        tabHost.setup(getActivity(), getChildFragmentManager());


        final TabWidget tw = (TabWidget)tabHost.findViewById(android.R.id.tabs);


        setupTabHost(tabHost, tabFragments);

        for (int i = 0; i < tw.getChildCount(); ++i)
        {
            final View tabView = tw.getChildTabViewAt(i);
            final TextView tv = (TextView)tabView.findViewById(android.R.id.title);
            tv.setTextSize(15);
            tv.setTextColor(getResources().getColor(R.color.blue));
            tv.setTypeface(Typeface.create("sans-serif-condensed", Typeface.BOLD));
        }

        viewPager.setCurrentItem(0, true);
        tabHost.setCurrentTab(0);
        return view;

    }

    private void setupTabHost(FragmentTabHost tabHost, List<TabFragment> tabFragments) {
        for(TabFragment fragment: tabFragments){
            tabHost.addTab(
                    tabHost.newTabSpec("tab" + tabFragments.indexOf(fragment)).setIndicator(fragment.getTitle(),
                            getResources().getDrawable(android.R.drawable.star_on)),
                    Fragment.class, null);
        }
    }

    /**
     * Initialize dashboard tabs fragments. Currently there are three tabs:
     * - Plan
     * - Stats
     * - Measure
     * @return
     */
    private List<TabFragment> initDashboardFragements(){
        if(this.tabFragments != null){
            return this.tabFragments;
        } else {
            List<TabFragment> tabFragments = new ArrayList<TabFragment>();
            tabFragments.add(MessagesTabFragment.instantiate(getString(R.string.messages)));
            tabFragments.add(InvitationsTabFragment.instantiate(getString(R.string.invitations)));
            tabFragments.add(Profile.instantiate(getString(R.string.settingss)));
            return tabFragments;
        }
    }

    @Override
    public void onTabChanged(String s) {
        int pos = this.tabHost.getCurrentTab();
        this.viewPager.setCurrentItem(pos, true);
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {
        this.tabHost.setCurrentTab(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
