var StudentResponse = require("../response/StudentResponse");
var _ = require("underscore");

function GroupResponse(group) {
  this.id = group._id;
  this.groupName = group.name;
  this.schoolName = group.description;
  this.teacherName = group.ownerProfile ? group.ownerProfile.name : "";
  this.students = [];
  this.addStudents(group.students);
}

GroupResponse.prototype.addStudent = function (student) {
  var that = this;
  that.students.push(new StudentResponse(student));
};

GroupResponse.prototype.addStudents = function (students) {
  var that = this;
  students.forEach(function (student) {
    that.addStudent(student);
  });
};

module.exports = GroupResponse;


