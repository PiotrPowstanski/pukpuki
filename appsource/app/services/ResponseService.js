var ResponseCreator = require("../common/ResponseCreator");

function ResponseService(response){
  this.response = response;
  return this;
}

ResponseService.prototype.send = function (object){
  this.response.setHeader('content-type', 'application/json');
  this.response.send(JSON.stringify(ResponseCreator.createResponse(object)));
}

ResponseService.prototype.sendError = function(error){
  this.response.send(JSON.stringify(ResponseCreator.createError(error)));
}

module.exports = ResponseService;
