function MemberCreationException(message) {
  this.message = message;
  this.name = "MemberCreationException";
}

module.exports = MemberCreationException;
