function ConfirmReadingException(message) {
  this.message = message;
  this.name = "ConfirmReadingException";
}

module.exports = ConfirmReadingException;
