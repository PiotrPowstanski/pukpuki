// app/routes.js
var MessagesService = new require('../app/services/MessagesService');
var ResponseService = require('../app/services/ResponseService');

module.exports = function(app, passport) {

    // route middleware to make sure
    function isLoggedIn(req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the home page
        res.redirect('/login');
    }

    // =====================================
    // Mobile Apis =========================
    // =====================================


    /**
     * healthcheck
     *
     */
    // ID Token
    //app.post('/api/mobile/test',
    //    passport.authenticate('google-id-token'),
    //    function (req, res) {
    //        console.log('/api/mobile/test');
    //        // do something with req.user
    //        res.send({'test': 'POST_HEALTHCHECK_OK'});
    //    }
    //);

    // ID Token
    app.post('/auth/google/idToken',
        passport.authenticate('google-id-token'),
        function (req, res) {
            console.log('/auth/google/idToken');
            // do something with req.user
            res.send({'test': 'POST_GOOGLE_ID_TOKEN_OK'});
        }
    );

    app.get('/api/mobile/test', isLoggedIn, function(req, res) {
        res.json({'test': 'GET_HEALTHCHECK_OK'});
    });



    /**
     * profile information an authorised user
     *
     */
     // ID Token
    app.post('/api/mobile/me',
        passport.authenticate('google-id-token'),
        function (req, res) {
            console.log('/api/mobile/me');
            res.json(req.user);
        });

    /**
     * messages for an authorised user
     *
     */
    // ID Token
    app.get('/api/mobile/inboundmessages', isLoggedIn, function (req, resp) {
            console.log('/api/mobile/inboundmessages');
            var email = req.user.email;
            MessagesService.getInboundMessages(email, function (messages, err) {
                var responseService = new ResponseService(resp);
                if (err) {
                    responseService.sendError(err);
                } else {
                    responseService.send(messages);
                }
            })
        });

}
