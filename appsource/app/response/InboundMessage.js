function InboundMessage(
  messageId,
  groupId,
  subject,
  text,
  creationDate,
  teacherName,
  studentFirstName,
  studentLastName,
  parentId

) {
  this.messageId = messageId;
  this.groupId = groupId;
  this.subject = subject;
  this.text = text;
  this.creationDate = creationDate;
  this.teacherName = teacherName;
  this.studentFirstName = studentFirstName;
  this.studentLasttName = studentLastName;
  this.parentId = parentId;
}

module.exports = InboundMessage;
