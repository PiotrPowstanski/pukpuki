module.exports = {
  createResponse : function(object){
    return {result : object};
  },
  createError : function(err){
    return {error : err};
  }
};
